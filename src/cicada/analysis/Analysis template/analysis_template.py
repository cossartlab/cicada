from cicada.analysis.cicada_analysis import CicadaAnalysis
from time import time

"""Change name of the class depending on file name"""


class CicadaAnalysisTemplate(CicadaAnalysis):
    def __init__(self, config_handler=None):
        """
        """
        CicadaAnalysis.__init__(self, name="[INSERT NAME]", family_id="[INSERT FAMILY ID]",
                                short_description="[INSERT SHORT DESCRIPTION]",
                                long_description="[INSERT LONG DESCRIPTION]",
                                config_handler=config_handler,
                                accepted_data_formats=["CI_DATA"])

    def copy(self):
        """
        Make a copy of the analysis
        Returns:

        """
        """Change name of the class"""
        analysis_copy = CicadaAnalysisTemplate(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """
        Check the data given one initiating the class and return True if the data given allows the analysis
        implemented, False otherwise.
        :return: a boolean
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_DATA":
                self.invalid_data_help = f"Non CI_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False
        """
        # other examples of verifications
        
        for data_to_analyse in self._data_to_analyse:
            roi_response_series = data_to_analyse.get_roi_response_series()
            if len(roi_response_series) == 0:
                self.invalid_data_help = f"No roi response series available in " \
                                         f"{data_to_analyse.identifier}"
                return False

        has_trial_table = []
        for data_to_analyse in self._data_to_analyse:
            has_trial_table.append(data_to_analyse.has_trial_table())
            if not all(has_trial_table):
                self.invalid_data_help = f"NWBs do not all have a trial table " \
                                         f"{data_to_analyse.identifier}"
                return False

        mice_list = []
        for data_to_analyse in self._data_to_analyse:
            mice_list.append(data_to_analyse.subject_id)
        if len(np.unique(mice_list)) <= 1:
            self.invalid_data_help = f"The selected NWBs are from only one mouse"
            return False
        """

        return True

    def set_arguments_for_gui(self):
        """

        Returns:

        """
        CicadaAnalysis.set_arguments_for_gui(self)
        """
        Add here the functions that allows the user to choose the parameters of the analysis.
        These functions are called 'add_...' and are defined in 'cicada_analysis.py' (cicada/src/cicada/analysis)
        
        For example: 
        
        plots_list = ['average_across_days', 'average_across_context_days', 'average_switch_time']
        self.add_choices_arg_for_gui(arg_name="plots_to_do", choices=plots_list,
                                     default_value="average_across_days", short_description="Plot(s) to do",
                                     multiple_choices=True,
                                     family_widget="plot type")
        """

    def update_original_data(self):
        """
        To be called if the data to analyse should be updated after the analysis has been run.
        :return: boolean: return True if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """
        test
        :param kwargs:
          segmentation

        :return:
        """
        CicadaAnalysis.run_analysis(self, **kwargs)
        """
        Here get the arguments you defined above
        
        For example:
        
        plots = kwargs.get('plots_to_do')
        """
        n_sessions = len(self._data_to_analyse)

        for session_index, session_data in enumerate(self._data_to_analyse):
            """
            Use wrappers to retrieve data from NWB files.
            These functions are defined in 'cicada_analysis_nwb_lsens_wrappers' but new ones can be created. 
            (cicada/src/cicada/analysis)
            
            For example:
            
            neuronal_data = session_data.get_roi_response_serie_data(keys=roi_response_serie_info)            
            neuronal_data_timestamps = np.asarray(session_data.get_roi_response_serie_timestamps(keys=roi_response_serie_info))
            # with roi_response_serie_info being a list : ['ophys', 'fluorescence', 'rrs_name']
            
            events_timestamps = session_data.get_behavioral_events_times(event_name=my_event_name)
            """

            """
            Create utils to do the analysis and plots
            """
            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / n_sessions)

        print(f"[INSERT TYPE OF ANALYSIS] run in {time() - self.analysis_start_time} sec")
