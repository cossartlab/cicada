import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import os

from cicada.analysis.cicada_analysis import CicadaAnalysis
from cicada.utils.misc.array_string_manip import print_info_dict, find_nearest
from cicada.utils.behavior.behavior_analysis_utils import (add_correct_choice_to_table, get_all_quiet_windows,
                                                           filter_table_with_dict, align_array_from_table_stim_time,
                                                           get_epoch_transition_list, plot_with_point_and_strip,
                                                           get_epoch_frames, get_frames_in_stim)
from cicada.utils.misc.periods import get_continous_time_periods
from cicada.utils.display.psth import align_array_from_timestamps_list, event_aligned_array_to_long_format_df
from cicada.utils.behavior.behavior_analysis_plot_functions import save_fig_and_close


class CicadaMovementQuantification(CicadaAnalysis):
    def __init__(self, config_handler=None):
        """
        """
        CicadaAnalysis.__init__(self, name="Facemap movement analysis", family_id="Behavior",
                                short_description="Quantify movements",
                                config_handler=config_handler, accepted_data_formats=["CI_DATA"])

    def copy(self):
        """
        Make a copy of the analysis
        Returns:

        """
        analysis_copy = CicadaMovementQuantification(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """
        Check the data given one initiating the class and return True if the data given allows the analysis
        implemented, False otherwise.
        :return: a boolean
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_DATA":
                self.invalid_data_help = f"Non CI_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        return True

    def set_arguments_for_gui(self):
        """

        Returns:

        """
        CicadaAnalysis.set_arguments_for_gui(self)

        plots_list = ['session_plot', 'mouse_plot', 'grand_average_plot']
        self.add_choices_arg_for_gui(arg_name="plots_to_do", choices=plots_list,
                                     default_value="grand_average_plot", short_description="Plot(s) to do",
                                     multiple_choices=True,
                                     family_widget="plot type")

        self.add_bool_option_for_gui(arg_name="only_correct_trials", true_by_default=False,
                                     short_description="Keep only correct trials in trial table",
                                     long_description=None, family_widget='Compare baseline config')

        self.add_choices_arg_for_gui(arg_name="quiet_window_mode",
                                     choices=['last 2s', 'last 0.5s', 'full quiet window'],
                                     default_value='last 2s',
                                     short_description="Define the quiet window to extract data at each trial",
                                     long_description="",
                                     multiple_choices=False,
                                     family_widget="Compare baseline config")

        if all([session_data.has_trial_table() for session_data in self._data_to_analyse]):
            self.add_trial_dict_arg_for_gui(short_description="Define trials selection from trial table",
                                            long_description=None,
                                            multiple_choices=True,
                                            arg_name='trial_table', family_widget="Trials selection")

        self.add_bool_option_for_gui(arg_name="normalized_motion_energy", true_by_default=False,
                                     short_description="Do Min Max normalization on facemotion energy",
                                     long_description=None, family_widget='Face motion')

        self.add_int_values_arg_for_gui(arg_name="n_svd_images", min_value=0, max_value=500,
                                        short_description="N SVD to keep",
                                        default_value=100, family_widget="Face motion")

        self.add_int_values_arg_for_gui(arg_name="t_before", min_value=0, max_value=2000,
                                        short_description="Time before stimulus or context transition (ms)",
                                        default_value=500, family_widget="Face motion PSTH")

        self.add_int_values_arg_for_gui(arg_name="t_after", min_value=0, max_value=5000,
                                        short_description="Time after stimulus or context transition (ms)",
                                        default_value=1000, family_widget="Face motion PSTH")

        self.add_image_format_package_for_gui()

        self.add_verbose_arg_for_gui()

    def update_original_data(self):
        """
        To be called if the data to analyse should be updated after the analysis has been run.
        :return: boolean: return True if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """
        test
        :param kwargs:
          segmentation

        :return:
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        plots_to_do = kwargs.get("plots_to_do")

        quiet_window_mode = kwargs.get("quiet_window_mode")

        only_correct_trials = kwargs.get("only_correct_trials")

        trial_selection = kwargs.get("trial_table")

        normalized_motion_energy = kwargs.get("normalized_motion_energy")

        n_svd_images = kwargs.get("n_svd_images")

        t_before = kwargs.get("t_before") / 1000

        t_after = kwargs.get("t_after") / 1000

        save_formats = kwargs["save_formats"]
        if save_formats is None:
            save_formats = ["pdf"]

        verbose = kwargs.get("verbose", True)

        # Define list of table to aggregate results
        baseline_data_df = []
        iti_motion_df = []
        full_stim_aligned_motion_df = []
        full_stim_aligned_motion_svd_df = []
        full_context_aligned_motion_df = []
        full_context_aligned_motion_svd_df = []
        for session_index, session_data in enumerate(self._data_to_analyse):
            # Get Session Info
            info_dict = session_data.get_sessions_info()
            session_identifier = info_dict['identifier']

            if verbose:
                print(f" ")
                print(f"------------------ ONGOING SESSION: {session_identifier} -------------------- ")
                print(f"----------------------------- SESSION INFO ---------------------------------- ")
                print_info_dict(info_dict)
                print(f" ")

            # Get facemap data
            facemap_data = session_data.get_facemap_data(image_acquisition_name="facemap_svd_images")
            if normalized_motion_energy:
                motion_energy = (facemap_data['motion_energy'] - np.min(facemap_data['motion_energy'])) / \
                                           np.max(facemap_data['motion_energy'])
            else:
                motion_energy = facemap_data['motion_energy']
            facemap_timestamps = facemap_data['timestamps']
            video_frame_rate = np.round(1 / np.median(np.diff(facemap_timestamps[0: 200])), 3)
            print(f"Behaviour video frame rate : {video_frame_rate} Hz")

            # Get trial table
            trial_table = session_data.get_trial_table()

            # Add column 'correct trial' (0 or 1) to filter it if needed
            trial_table = add_correct_choice_to_table(trial_table, colname='correct_trial')
            if only_correct_trials:
                trial_table = trial_table.loc[trial_table.correct_trial == True]
                trial_table = trial_table.reset_index(drop=True)

            # Get all quiet windows
            quiet_window_frames = get_all_quiet_windows(table=trial_table,
                                                        reference_ts=facemap_timestamps,
                                                        quiet_window_mode=quiet_window_mode)

            # Get averaged face motion energy for each quiet window
            avg_motion_energy = [np.nanmean(motion_energy[quiet_frames]) for quiet_frames in quiet_window_frames]

            # Add it to trial table
            trial_table['BaselineMotionEnergy'] = avg_motion_energy

            # Add mouse / session_info before concatenation
            trial_table['session_id'] = session_identifier
            trial_table['mouse_id'] = session_identifier[0: 5]

            # Filter trial table for baseline face motion energy plot
            cols_to_keep = ['mouse_id', 'session_id', 'trial_type', 'correct_trial', 'context', 'BaselineMotionEnergy']
            baseline_trial_table = trial_table[cols_to_keep]

            # Aggregate table with baseline face motion energy
            baseline_data_df.append(baseline_trial_table)

            # Find ITI frames of R+ and R- to average face motion energy
            iti_rew_frames = np.ones(len(facemap_timestamps)).astype(bool)
            iti_non_rew_frames = np.ones(len(facemap_timestamps)).astype(bool)
            rewarded_times = session_data.get_behavioral_epochs_times(epoch_name='rewarded')
            non_rewarded_times = session_data.get_behavioral_epochs_times(epoch_name='non-rewarded')
            rewarded_epoch_frames = get_epoch_frames(epoch_times=rewarded_times,
                                                     reference_ts=facemap_timestamps,
                                                     max_idx=len(facemap_timestamps))
            non_rewarded_epoch_frames = get_epoch_frames(epoch_times=non_rewarded_times,
                                                         reference_ts=facemap_timestamps,
                                                         max_idx=len(facemap_timestamps))
            iti_non_rew_frames[rewarded_epoch_frames] = False
            iti_rew_frames[non_rewarded_epoch_frames] = False
            stim_frames = get_frames_in_stim(trial_table=trial_table,
                                             stim_names=['whisker_trial', 'auditory_trial'],
                                             trial_duration=5,
                                             reference_ts=facemap_timestamps)
            iti_rew_frames[stim_frames] = False
            iti_rew = get_continous_time_periods(iti_rew_frames)
            iti_non_rew_frames[stim_frames] = False
            iti_non_rew = get_continous_time_periods(iti_non_rew_frames)
            iti_table = pd.DataFrame()

            iti_rew_mot = []
            for i_rew in iti_rew:
                if i_rew[0] == i_rew[1]:
                    continue
                iti_rew_mot.append(np.nanmean(motion_energy[np.arange(i_rew[0],
                                                                      min(i_rew[1], len(motion_energy)))]))

            iti_non_rew_mot = []
            for i_nnrew in iti_non_rew:
                if i_nnrew[0] == i_nnrew[1]:
                    continue
                iti_non_rew_mot.append(np.nanmean(motion_energy[np.arange(i_nnrew[0],
                                                                          min(i_nnrew[1], len(motion_energy)))]))

            context_vec = ['R+'] * len(iti_rew_mot) + ['R-'] * len(iti_non_rew_mot)
            iti_table['motion'] = np.concatenate((iti_rew_mot, iti_non_rew_mot))
            iti_table['context'] = context_vec
            iti_table['session_id'] = session_identifier
            iti_table['mouse_id'] = session_identifier[0: 5]
            iti_motion_df.append(iti_table)

            # Get filtered table with specific trials from selection dict
            sub_table = filter_table_with_dict(table=trial_table, filtering_dict=trial_selection)
            sub_table = sub_table.reset_index(drop=True)

            # Align individual SVDs to stim time from filtered table
            stim_aligned_motion_svd = align_array_from_table_stim_time(table=sub_table,
                                                                       array=facemap_data['motion_svd'],
                                                                       array_ts=facemap_timestamps,
                                                                       t_before=t_before,
                                                                       t_after=t_after)
            stim_aligned_motion_svd_df = event_aligned_array_to_long_format_df(array=stim_aligned_motion_svd,
                                                                               time_range=[-t_before, t_after],
                                                                               time_step=1 / video_frame_rate)
            stim_aligned_motion_svd_df['session_id'] = session_identifier
            stim_aligned_motion_svd_df['mouse_id'] = session_identifier[0: 5]
            stim_aligned_motion_svd_df = stim_aligned_motion_svd_df.loc[stim_aligned_motion_svd_df['sample'] < n_svd_images]
            full_stim_aligned_motion_svd_df.append(stim_aligned_motion_svd_df)

            # Align face motion energy to stim time from filtered table
            stim_aligned_motion = align_array_from_table_stim_time(table=sub_table,
                                                                   array=motion_energy,
                                                                   array_ts=facemap_timestamps,
                                                                   t_before=t_before,
                                                                   t_after=t_after)
            stim_aligned_motion_df = event_aligned_array_to_long_format_df(array=stim_aligned_motion,
                                                                           time_range=[-t_before, t_after],
                                                                           time_step=1 / video_frame_rate)
            stim_aligned_motion_df['session_id'] = session_identifier
            stim_aligned_motion_df['mouse_id'] = session_identifier[0: 5]
            full_stim_aligned_motion_df.append(stim_aligned_motion_df)

            # Get aligned activity around transition to R+ and to R-
            rewarded_times = session_data.get_behavioral_epochs_times(epoch_name='rewarded')
            non_rewarded_times = session_data.get_behavioral_epochs_times(epoch_name='non-rewarded')
            to_rewarded_times = get_epoch_transition_list(epoch_times=rewarded_times)
            to_non_rewarded_times = get_epoch_transition_list(epoch_times=non_rewarded_times)

            # Transition to R+
            # Align individual SVDs to R+ transitions
            to_rew_aligned_motion_svd = align_array_from_timestamps_list(array=facemap_data['motion_svd'],
                                                                         array_ts=facemap_timestamps,
                                                                         timestamps_list=to_rewarded_times,
                                                                         t_before=t_before,
                                                                         t_after=t_after)
            to_rew_aligned_motion_svd_df = event_aligned_array_to_long_format_df(array=to_rew_aligned_motion_svd,
                                                                                 time_range=[-t_before, t_after],
                                                                                 time_step=1 / video_frame_rate)
            to_rew_aligned_motion_svd_df['session_id'] = session_identifier
            to_rew_aligned_motion_svd_df['mouse_id'] = session_identifier[0: 5]
            to_rew_aligned_motion_svd_df['transition'] = 'To R+'

            # Align face motion energy to R+ transitions
            to_rew_aligned_motion = align_array_from_timestamps_list(array=motion_energy,
                                                                     array_ts=facemap_timestamps,
                                                                     timestamps_list=to_rewarded_times,
                                                                     t_before=t_before,
                                                                     t_after=t_after)
            to_rew_aligned_motion_df = event_aligned_array_to_long_format_df(array=to_rew_aligned_motion,
                                                                             time_range=[-t_before, t_after],
                                                                             time_step=1 / video_frame_rate)
            to_rew_aligned_motion_df['session_id'] = session_identifier
            to_rew_aligned_motion_df['mouse_id'] = session_identifier[0: 5]
            to_rew_aligned_motion_df['transition'] = 'To R+'

            # Transition to R-
            # Align individual SVDs to R- transition
            to_nn_rew_aligned_motion_svd = align_array_from_timestamps_list(array=facemap_data['motion_svd'],
                                                                            array_ts=facemap_timestamps,
                                                                            timestamps_list=to_non_rewarded_times,
                                                                            t_before=t_before,
                                                                            t_after=t_after)
            to_nn_rew_aligned_motion_svd_df = event_aligned_array_to_long_format_df(array=to_nn_rew_aligned_motion_svd,
                                                                                    time_range=[-t_before, t_after],
                                                                                    time_step=1 / video_frame_rate)
            to_nn_rew_aligned_motion_svd_df['session_id'] = session_identifier
            to_nn_rew_aligned_motion_svd_df['mouse_id'] = session_identifier[0: 5]
            to_nn_rew_aligned_motion_svd_df['transition'] = 'To R-'

            # Align face motion energy to R- transitions
            to_nn_rew_aligned_motion = align_array_from_timestamps_list(array=motion_energy,
                                                                        array_ts=facemap_timestamps,
                                                                        timestamps_list=to_non_rewarded_times,
                                                                        t_before=t_before,
                                                                        t_after=t_after)
            to_nn_rew_aligned_motion_df = event_aligned_array_to_long_format_df(array=to_nn_rew_aligned_motion,
                                                                                time_range=[-t_before, t_after],
                                                                                time_step=1 / video_frame_rate)
            to_nn_rew_aligned_motion_df['session_id'] = session_identifier
            to_nn_rew_aligned_motion_df['mouse_id'] = session_identifier[0: 5]
            to_nn_rew_aligned_motion_df['transition'] = 'To R-'

            # Concatenate df for SVDs & face motion context transitions
            context_aligned_motion_svd_df = pd.concat((to_rew_aligned_motion_svd_df, to_nn_rew_aligned_motion_svd_df), ignore_index=True)
            context_aligned_motion_df = pd.concat((to_rew_aligned_motion_df, to_nn_rew_aligned_motion_df), ignore_index=True)

            context_aligned_motion_svd_df = context_aligned_motion_svd_df.loc[context_aligned_motion_svd_df['sample'] < n_svd_images]
            full_context_aligned_motion_svd_df.append(context_aligned_motion_svd_df)
            context_aligned_motion_df = context_aligned_motion_df.loc[context_aligned_motion_df['sample'] < n_svd_images]
            full_context_aligned_motion_df.append(context_aligned_motion_df)

            # Make face motion svd images figures
            n_rows = int(np.sqrt(n_svd_images))
            n_cols = int(np.sqrt(n_svd_images))
            fig, axes = plt.subplots(n_rows, n_cols, figsize=(10, 10), sharex=True, sharey=True)
            for idx, ax in enumerate(axes.flatten()):
                ax.imshow(facemap_data['svd_images'][idx, :, :])
                ax.spines[['left', 'bottom', 'right', 'top']].set_visible(False)
                ax.set_title(f'SVD {idx}', fontsize=6)
                ax.tick_params(axis='both', which='both', labelsize=6)
            save_fig_and_close(results_path=os.path.join(self.get_results_path(), info_dict['subject_id']),
                               save_formats=save_formats,
                               figure=fig, figname=f'{session_identifier}_motion_svd_images')

            self.update_progressbar(time_started=self.analysis_start_time,
                                    increment_value=100 / len(self._data_to_analyse))

        # Get table from lists
        baseline_data_df = pd.concat(baseline_data_df)
        iti_motion_df = pd.concat(iti_motion_df)
        full_stim_aligned_motion_df = pd.concat(full_stim_aligned_motion_df)
        full_stim_aligned_motion_svd_df = pd.concat(full_stim_aligned_motion_svd_df)
        full_context_aligned_motion_df = pd.concat(full_context_aligned_motion_df)
        full_context_aligned_motion_svd_df = pd.concat(full_context_aligned_motion_svd_df)

        # Save the datasets
        baseline_data_df.to_csv(os.path.join(self.get_results_path(), 'baseline_energy.csv'))
        iti_motion_df.to_csv(os.path.join(self.get_results_path(), 'iti_energy.csv'))
        full_stim_aligned_motion_df.to_csv(os.path.join(self.get_results_path(), 'stim_aligned_energy.csv'))
        full_stim_aligned_motion_svd_df.to_csv(os.path.join(self.get_results_path(), 'stim_aligned_svd_energy.csv'))
        full_context_aligned_motion_df.to_csv(os.path.join(self.get_results_path(), 'context_aligned_energy.csv'))
        full_context_aligned_motion_svd_df.to_csv(os.path.join(self.get_results_path(), 'context_aligned_svd_energy.csv'))
        if verbose:
            print('Dataset saved')

        if verbose:
            print(' ')
            print('Do figures')
        context_palette_dict = {'context': ['darkmagenta', 'green']}
        if "session_plot" in plots_to_do:
            for session_id in baseline_data_df.session_id.unique():
                # Face motion energy during quiet window
                baseline_data_to_plot = baseline_data_df.loc[baseline_data_df.session_id == session_id]
                fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                plot_with_point_and_strip(data=baseline_data_to_plot, x_name='trial_type',
                                          y_name='BaselineMotionEnergy', hue='context', legend=True,
                                          palette=context_palette_dict, ax=ax, palette_key='context', link_mice=False)
                sns.despine()
                save_fig_and_close(results_path=os.path.join(self.get_results_path(), session_id[0:5]),
                                   save_formats=save_formats,
                                   figure=fig, figname=f'{session_id}_quiet_window_motion_energy')

                # Face Motion trial based PSTHs
                face_motion_df_to_plot = full_stim_aligned_motion_df.loc[full_stim_aligned_motion_df.session_id == session_id]
                fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                sns.lineplot(data=face_motion_df_to_plot, x='time', y='data', color='grey', ax=ax)
                sns.despine()
                ax.axvline(x=0, ymin=0, ymax=1, c='orange', linestyle='--')
                ax.tick_params(axis='both', which='both', labelsize=6)
                ax.set_title('Face Motion', fontsize=6)
                ax.set_ylabel('Face motion energy', fontsize=6)
                ax.set_xlabel('Time (s)', fontsize=6)
                save_fig_and_close(results_path=os.path.join(self.get_results_path(), session_id[0:5]),
                                   save_formats=save_formats,
                                   figure=fig, figname=f'{session_id}_psth_face_motion_stim')

                # SVD trial based PSTHs
                n_rows = int(np.sqrt(n_svd_images))
                n_cols = int(np.sqrt(n_svd_images))
                svd_session_df = full_stim_aligned_motion_svd_df.loc[full_stim_aligned_motion_svd_df.session_id == session_id]
                fig, axes = plt.subplots(n_rows, n_cols, figsize=(10, 10), sharex=True, sharey=True)
                for idx, ax in enumerate(axes.flatten()):
                    df_to_plot = svd_session_df.loc[svd_session_df['sample'] == idx]
                    sns.lineplot(data=df_to_plot, x='time', y='data', color='grey', ax=ax)
                    sns.despine()
                    ax.axvline(x=0, ymin=0, ymax=1, c='orange', linestyle='--')
                    ax.spines[['right', 'top']].set_visible(False)
                    ax.set_title(f'SVD {idx}', fontsize=6)
                    ax.set_ylabel('Avg energy', fontsize=6)
                    ax.set_xlabel('Time (s)', fontsize=6)
                    ax.tick_params(axis='both', which='both', labelsize=6)
                save_fig_and_close(results_path=os.path.join(self.get_results_path(), session_id[0:5]),
                                   save_formats=save_formats,
                                   figure=fig, figname=f'{session_id}_psth_svd')

                # Face Motion context transition aligned
                face_motion_df_to_plot = full_context_aligned_motion_df.loc[full_context_aligned_motion_df.session_id == session_id]
                fig, ax = plt.subplots(1, 1, figsize=(5, 5))
                sns.lineplot(data=face_motion_df_to_plot, x='time', y='data', color='grey',
                             hue='transition', hue_order=['To R-', 'To R+'], palette=context_palette_dict['context'],
                             ax=ax)
                sns.despine()
                ax.axvline(x=0, ymin=0, ymax=1, c='grey', linestyle='--')
                ax.spines[['right', 'top']].set_visible(False)
                ax.tick_params(axis='both', which='both', labelsize=6)
                ax.set_title('Face Motion', fontsize=6)
                ax.set_ylabel('Avg energy', fontsize=6)
                ax.set_xlabel('Time (s)', fontsize=6)
                save_fig_and_close(results_path=os.path.join(self.get_results_path(), session_id[0:5]),
                                   save_formats=save_formats,
                                   figure=fig, figname=f'{session_id}_psth_face_motion_context_transition')

                # SVD context transition PSTHs
                fig, axes = plt.subplots(n_rows, n_cols, figsize=(10, 10), sharex=True, sharey=True)
                session_data = full_context_aligned_motion_svd_df.loc[full_context_aligned_motion_svd_df.session_id == session_id]
                for idx, ax in enumerate(axes.flatten()):
                    df_to_plot = session_data.loc[session_data['sample'] == idx]
                    sns.lineplot(data=df_to_plot, x='time', y='data', color='grey',
                                 hue='transition', hue_order=['To R-', 'To R+'],
                                 palette=context_palette_dict['context'], legend=False,
                                 ax=ax)
                    sns.despine()
                    ax.axvline(x=0, ymin=0, ymax=1, c='grey', linestyle='--')
                    ax.set_title(f'SVD {idx}', fontsize=6)
                    ax.set_ylabel('Avg energy', fontsize=6)
                    ax.set_xlabel('Time (s)', fontsize=6)
                    ax.tick_params(axis='both', which='both', labelsize=6)
                save_fig_and_close(results_path=os.path.join(self.get_results_path(), session_id[0:5]),
                                   save_formats=save_formats,
                                   figure=fig, figname=f'{session_id}_psth_svd_context')





