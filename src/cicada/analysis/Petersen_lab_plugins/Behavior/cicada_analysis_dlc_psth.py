import os
import seaborn as sns
import numpy as np
import pandas as pd

from cicada.analysis.cicada_analysis import CicadaAnalysis
from time import time

from cicada.utils.behavior.DLC_PSTH import filter_events_based_on_epochs, plot_psth_dlc, create_trial_df
from cicada.utils.behavior.behavior_analysis_plot_functions import save_fig_and_close
from cicada.utils.misc.array_string_manip import print_info_dict, find_nearest


class CicadaAnalysisDlcPsth(CicadaAnalysis):
    def __init__(self, config_handler=None):
        """
        """
        long_description = '<p align="center"><b>DLC PSTH</b></p>'
        long_description = long_description + 'Main parameters:<br>'
        long_description = long_description + (' - Box "events": Choose the type of event on which you want to center '
                                               'the PSTH (auditory hit, whisker miss, rewarded epoch start, ...).<br>')
        long_description = long_description + (' - Box" main epochs": You can filter the trials chosen based on the '
                                               'epoch.<br>')
        long_description = long_description + ' - Box "body_parts": Select the bodyparts to use for the PSTH.<br><br>'
        long_description = long_description + ("Table saved: All event's data with columns specifying mouse_id, "
                                               "session_id, bodypart, time, trial_name and epoch.<br><br>")
        long_description = long_description + ('Plot: PSTH with lines corresponding to bodyparts and columns to '
                                               'sessions.<br>')
        CicadaAnalysis.__init__(self, name="Deep Lab Cut PSTH", family_id="Behavior",
                                short_description="Build PeriStimuli Time Histogram at population level, for DLC data",
                                config_handler=config_handler, accepted_data_formats=["CI_DATA"],
                                long_description=long_description)

    def copy(self):
        """
        Make a copy of the analysis
        Returns:

        """
        analysis_copy = CicadaAnalysisDlcPsth(config_handler=self.config_handler)
        self.transfer_attributes_to_tabula_rasa_copy(analysis_copy=analysis_copy)
        return analysis_copy

    def check_data(self):
        """
        Check the data given one initiating the class and return True if the data given allows the analysis
        implemented, False otherwise.
        :return: a boolean
        """
        super().check_data()

        for session_index, session_data in enumerate(self._data_to_analyse):
            if session_data.DATA_FORMAT != "CI_DATA":
                self.invalid_data_help = f"Non CI_DATA format compatibility not yet implemented: " \
                                         f"{session_data.DATA_FORMAT}"
                return False

        all_behavior_timeseries = []
        for session_index, session_data in enumerate(self._data_to_analyse):
            all_behavior_timeseries.extend(session_data.get_behavioral_time_series_names())
        all_behavior_timeseries = list(np.unique(all_behavior_timeseries))
        if not all_behavior_timeseries:
            self.invalid_data_help = f"No DLC data in files"
            return False

        return True

    def set_arguments_for_gui(self):
        """

        Returns:

        """
        CicadaAnalysis.set_arguments_for_gui(self)

        plots_list = ['dlc_psth_session_plot', 'dlc_psth_mouse_plot', 'dlc_psth_full_plot']
        self.add_choices_arg_for_gui(arg_name="plots_to_do", choices=plots_list,
                                     default_value="wf_psth_full_plot", short_description="Plot(s) to do",
                                     multiple_choices=True,
                                     family_widget="plot type")
        display_list = ['epoch', 'trial_name']
        self.add_choices_arg_for_gui(arg_name="to_display", choices=display_list,
                                     default_value=False, short_description="Display(s) to do",
                                     multiple_choices=True,
                                     long_description='If one is selected, it is used as hue, if the 2 are selected, '
                                                      'epoch is the hue, trial type is the style',
                                     family_widget="plot type")

        all_events = []
        all_epochs = []
        all_behavior_timeseries = []
        for data_to_analyse in self._data_to_analyse:
            all_events.extend(data_to_analyse.get_behavioral_epochs_names())
            all_epochs.extend(data_to_analyse.get_behavioral_epochs_names())
            all_events.extend(data_to_analyse.get_behavioral_events_names())
            all_behavior_timeseries.extend(data_to_analyse.get_behavioral_time_series_names())

        all_events = list(np.unique(all_events))
        all_events = [event for event in all_events if 'dlc' not in event]
        all_epochs = list(np.unique(all_epochs))
        all_behavior_timeseries = list(np.unique(all_behavior_timeseries))

        self.add_choices_arg_for_gui(arg_name="epochs", choices=all_epochs,
                                     default_value=False,
                                     short_description="Select epoch(s) to build PSTH",
                                     long_description="epochs selected are used to filter events",
                                     multiple_choices=True,
                                     family_widget="main epochs")

        self.add_choices_arg_for_gui(arg_name="events_names", choices=all_events,
                                     default_value=False,
                                     short_description="Behavioral Epochs or Events",
                                     long_description="Select events for which you want to build PSTH",
                                     multiple_choices=True,
                                     family_widget="events")

        self.add_choices_arg_for_gui(arg_name="body_part", choices=all_behavior_timeseries,
                                     default_value=False,
                                     short_description="Select body part to analyse",
                                     long_description="",
                                     multiple_choices=True,
                                     family_widget="body_parts")

        self.add_int_values_arg_for_gui(arg_name="psth_start", min_value=50, max_value=20000,
                                        short_description="Range of the PSTH (ms) before event",
                                        default_value=250, family_widget="psth_config")
        self.add_int_values_arg_for_gui(arg_name="psth_stop", min_value=50, max_value=20000,
                                        short_description="Range of the PSTH (ms) after event",
                                        default_value=250, family_widget="psth_config")

        # self.add_color_arg_for_gui(arg_name="session_default_color", default_value=(0, 0, 0, 1.),
        #                            short_description="Default color for session",
        #                            long_description=None, family_widget="session_color_config")
        #
        # self.add_int_values_arg_for_gui(arg_name="psth_range", min_value=50, max_value=20000,
        #                                 short_description="Range of the PSTH (ms)",
        #                                 default_value=10000, family_widget="psth_config")

        self.add_image_format_package_for_gui()

        self.add_verbose_arg_for_gui()

    def update_original_data(self):
        """
        To be called if the data to analyse should be updated after the analysis has been run.
        :return: boolean: return True if the data has been modified
        """
        pass

    def run_analysis(self, **kwargs):
        """
        test
        :param kwargs:
          segmentation

        :return:
        """
        CicadaAnalysis.run_analysis(self, **kwargs)

        verbose = kwargs.get("verbose", True)

        to_display = kwargs.get("to_display")
        hue = None if not to_display else to_display[0]
        style = None if len(to_display) < 2 else to_display[1]

        plots = kwargs.get('plots_to_do')

        epochs = kwargs.get("epochs")
        events_names = kwargs.get("events_names")
        body_parts = kwargs.get("body_part")

        psth_start_in_ms = kwargs.get("psth_start")
        psth_start_in_sec = psth_start_in_ms / 1000
        psth_stop_in_ms = kwargs.get("psth_stop")
        psth_stop_in_sec = psth_stop_in_ms / 1000

        # ------- figures config part -------
        save_formats = kwargs["save_formats"]
        if save_formats is None:
            save_formats = "pdf"

        dpi = kwargs.get("dpi", 100)

        width_fig = kwargs.get("width_fig")

        height_fig = kwargs.get("height_fig")

        n_sessions = len(self._data_to_analyse)

        all_data_list = []

        for session_index, session_data in enumerate(self._data_to_analyse):
            # Get Session Info
            info_dict = session_data.get_sessions_info()
            session_identifier = info_dict['identifier']

            if verbose:
                print(f" ")
                print(f"------------------ ONGOING SESSION: {session_identifier} -------------------- ")
                print(f"----------------------------- SESSION INFO ---------------------------------- ")
                print_info_dict(info_dict)
                print(f" ")

            if not events_names:
                print("no events, no analysis")
                return

            behavior_movie_timestamps = session_data.get_behavioral_time_series_timestamps(body_parts[0])
            if behavior_movie_timestamps is None or len(behavior_movie_timestamps) == 0:
                print("no behavior_movie_timestamps, skip")
                continue

            behavior_movie_frame_rate = 1 / np.median(np.diff(behavior_movie_timestamps[:100]))

            if epochs:
                for epoch in epochs:
                    epoch_times = session_data.get_behavioral_epochs_times(epoch_name=epoch)
                    for events_name in events_names:
                        trials = session_data.get_behavioral_events_times(event_name=events_name)
                        if trials is None or len(trials) == 0:
                            trials = session_data.get_behavioral_epochs_times(epoch_name=epoch)
                        trials = trials[0, :]
                        trials_kept = filter_events_based_on_epochs(trials, epoch_times, verbose)
                        if len(trials_kept) == 0:
                            print("No trials in this condition, skipping")
                            continue

                        for tstamp in trials_kept:
                            start_time = max(0, tstamp - psth_start_in_sec)
                            start_frame_index = find_nearest(value=start_time, array=behavior_movie_timestamps)
                            stop_time = min(tstamp + psth_stop_in_sec, behavior_movie_timestamps[-1])
                            stop_frame_index = find_nearest(value=stop_time, array=behavior_movie_timestamps)

                            baseline_frame_duration = int(behavior_movie_frame_rate * psth_start_in_sec)

                            for body_part in body_parts:
                                bodypart_dataframe = create_trial_df(session_data, body_part, start_frame_index,
                                                                     stop_frame_index, baseline_frame_duration,
                                                                     psth_start_in_sec, psth_stop_in_sec, events_name,
                                                                     epoch)
                                all_data_list.append(bodypart_dataframe)
            else:
                # data centered on epochs
                for events_name in events_names:
                    trials = session_data.get_behavioral_events_times(event_name=events_name)
                    trials = trials[0, :]
                    if len(trials) == 0:
                        print("No trials in this condition, skipping")
                        continue

                    for tstamp in trials:
                        start_time = max(0, tstamp - psth_start_in_sec)
                        start_frame_index = find_nearest(value=start_time, array=behavior_movie_timestamps)
                        stop_time = min(tstamp + psth_stop_in_sec, behavior_movie_timestamps[-1])
                        stop_frame_index = find_nearest(value=stop_time, array=behavior_movie_timestamps)

                        baseline_frame_duration = int(behavior_movie_frame_rate * psth_start_in_sec)

                        for body_part in body_parts:
                            bodypart_dataframe = create_trial_df(session_data, body_part, start_frame_index,
                                                                 stop_frame_index, baseline_frame_duration,
                                                                 psth_start_in_sec, psth_stop_in_sec, events_name,
                                                                 epoch='not_defined')
                            all_data_list.append(bodypart_dataframe)

            self.update_progressbar(time_started=self.analysis_start_time, increment_value=100 / n_sessions)

        # Make general table
        all_data_dataframe = pd.concat(all_data_list, axis=0, ignore_index=True)
        full_psth_data_to_save_name = f"dlc_psth"
        path_results = self.get_results_path()
        full_path_psth_data_to_save = os.path.join(f'{path_results}',
                                                   f'{full_psth_data_to_save_name}.xlsx')

        if verbose:
            print(' ')
            print('Save the dataset')
        all_data_dataframe.to_excel(full_path_psth_data_to_save)

        # Do the plot
        if verbose:
            print(' ')
            print("Do the plot")

        # sessions plots
        if 'dlc_psth_session_plot' in plots:
            session_list = pd.unique(all_data_dataframe.session_id)
            for session in session_list:
                if verbose:
                    print(' ')
                    print(f'Do the session plot: {session}')
                session_table = all_data_dataframe.loc[all_data_dataframe['session_id'] == session]
                path = os.path.join(path_results, session[:5])
                if not os.path.exists(path):
                    os.makedirs(path)
                plot_psth_dlc(data=session_table, x='time', y='behavior_movie_data_event',
                              col='session_id', row='bodypart', hue=hue, style=style,
                              color_plot='blue', axes_label_color='white',
                              axes_border_color='white', color_ticks='white', background_color='black',
                              figsize=(15, 10), ticks_labels_size=5, axis_label_size=30, axis_label_pad=20,
                              save_path=path, save_formats=save_formats, file_name=f'{session}_widefield_psth')

        # mice plots
        dfs_list_session = all_data_dataframe.groupby(["mouse_id", "session_id", "trial_name", "epoch",
                                                       "bodypart", "time"],
                                                      as_index=False).agg(np.nanmean)
        if 'dlc_psth_mouse_plot' in plots:
            mouse_list = pd.unique(all_data_dataframe.mouse_id)
            for mouse in mouse_list:
                if verbose:
                    print(' ')
                    print(f'Do the mouse plot: {mouse}')
                mouse_table = dfs_list_session.loc[dfs_list_session['mouse_id'] == mouse]
                path = os.path.join(path_results, mouse)
                if not os.path.exists(path):
                    os.makedirs(path)
                plot_psth_dlc(data=mouse_table, x='time', y='behavior_movie_data_event',
                              col='mouse_id', row='bodypart', hue=hue, style=style,
                              color_plot='blue', axes_label_color='white',
                              axes_border_color='white', color_ticks='white', background_color='black',
                              figsize=(15, 10), ticks_labels_size=5, axis_label_size=30, axis_label_pad=20,
                              save_path=path, save_formats=save_formats, file_name=f'{mouse}_widefield_psth_average')

        # Full plot
        if 'dlc_psth_full_plot' in plots:
            if verbose:
                print(' ')
                print(f'Do the full plot')
            mice_avg_data = dfs_list_session.drop(['session_id'], axis=1)
            mice_avg_data = mice_avg_data.groupby(["mouse_id", "trial_name", "epoch", "bodypart", "time"],
                                                  as_index=False).agg(np.nanmean)
            plot_psth_dlc(data=mice_avg_data, x='time', y='behavior_movie_data_event', col=None, row='bodypart',
                          color_plot='blue', axes_label_color='white', hue=hue, style=style,
                          axes_border_color='white', color_ticks='white', background_color='black',
                          figsize=(15, 10), ticks_labels_size=5, axis_label_size=30, axis_label_pad=20,
                          save_path=path_results, save_formats=save_formats, file_name="psth_dlc")

        print(' ')
        print(f"DLC PSTH ANALYSIS run in {time() - self.analysis_start_time} sec")
