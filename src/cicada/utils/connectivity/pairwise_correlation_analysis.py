import numpy as np
import scipy.spatial.distance as sci_sp_dist
import scipy.stats
import matplotlib.pyplot as plt
import seaborn as sns
import math
import os
from datetime import datetime

import cicada.utils.misc.array_string_manip


def build_pearson_adjacency_matrix(neuronal_data, with_thresholds=True, correlation_threshold=0.1,
                                   p_value_threshold=0.05, verbose=0, data_id="", results_path=None):
    """
    Build the adjacency matrix of neuronal data
    Args:
        neuronal_data: 2d array of shape (n_cells x n_times)
        with_thresholds: use or not the following thresholds
        correlation_threshold: value between 0 and 1, correlation under the threshold will be put ot 0
        p_value_threshold: correlation with p > threshold will be put to 0
        verbose: (int) if > 0, some information will be printed
        data_id: describe the data, useful if verbose > 0 or results_path is not None
        results_path: if not None, (str) representing the path where to save the adjacency matrix, in numpy format

    Returns:

    """
    n_cells = neuronal_data.shape[0]

    adjacency_matrix = np.zeros((n_cells, n_cells))

    n_corr_kept = 0
    n_corr_total = 0
    if verbose > 0:
        print(f"Building pearson adjacency matrix for {data_id}")
    for first_cell in np.arange(n_cells):
        for second_cell in np.arange(first_cell, n_cells):
            # TODO: verify if the matrix should be symmetric, in that case no need calculate
            #  the correlation over all pairs
            if first_cell == second_cell:
                adjacency_matrix[first_cell, second_cell] = 1
                continue
            rho, p_value = scipy.stats.pearsonr(neuronal_data[first_cell], neuronal_data[second_cell])
            n_corr_total += 1
            if with_thresholds:
                if (p_value > p_value_threshold) or (rho <= correlation_threshold):
                    continue
                else:
                    adjacency_matrix[first_cell, second_cell] = rho
                    adjacency_matrix[second_cell, first_cell] = rho
            else:
                adjacency_matrix[first_cell, second_cell] = rho
                adjacency_matrix[second_cell, first_cell] = rho
            n_corr_kept += 1
    if with_thresholds:
        if verbose > 0:
            print(f"{n_corr_kept} correlation passed the threshold over a total of {n_corr_total}\n")

    if results_path is not None:
        np.save(os.path.join(results_path, f"pearson_adjacency_matrix_{data_id}.npy"), adjacency_matrix)

    return adjacency_matrix


def get_pairwise_hamming_similarity(neuronal_data):
    """
    Distribution of pair-wise Hamming distance of all cells
    :param neuronal_data: Must be binary matrix n_cells x n_frames (activity of the cell is 1, otherwise 0)
    :return:
    """
    n_cells = neuronal_data.shape[0]
    similarity_matrix = np.zeros((n_cells, n_cells), dtype=float)
    for cell_1 in range(n_cells):
        for cell_2 in np.arange(cell_1, n_cells):
            hamm_dist = sci_sp_dist.hamming(neuronal_data[cell_1], neuronal_data[cell_2])
            similarity = 1 - hamm_dist
            similarity_matrix[cell_1, cell_2] = similarity
            similarity_matrix[cell_2, cell_1] = similarity
    return similarity_matrix


def get_pairwise_jaccard_similarity(neuronal_data):
    n_cells = neuronal_data.shape[0]
    similarity_matrix = np.zeros((n_cells, n_cells), dtype=float)
    for cell_1 in range(n_cells):
        for cell_2 in np.arange(cell_1, n_cells):
            jaccard_dist = sci_sp_dist.jaccard(neuronal_data[cell_1], neuronal_data[cell_2])
            similarity = 1 - jaccard_dist
            similarity_matrix[cell_1, cell_2] = similarity
            similarity_matrix[cell_2, cell_1] = similarity
    return similarity_matrix


def compute_similarity_matrix(neuronal_data=None, method=None, verbose=False):
    if verbose:
        print(f"Starting to compute similarity matrix using {method} method")

    method_functions = {
        "Pearson": lambda: build_pearson_adjacency_matrix(neuronal_data=neuronal_data, with_thresholds=False,
                                                          verbose=0),
        "Hamming": lambda: get_pairwise_hamming_similarity(neuronal_data),
        "Jaccard": lambda: get_pairwise_jaccard_similarity(neuronal_data)
    }

    similarity_matrix = method_functions[method]()

    if verbose:
        print("Similarity matrix computation completed")

    return similarity_matrix


# UTILS
def create_and_configure_plot(n_lines, n_cols, size_fig, background_color):
    fig, axes = plt.subplots(nrows=n_lines, ncols=n_cols, figsize=size_fig,
                             gridspec_kw={'width_ratios': [1] * n_cols, 'height_ratios': [1] * n_lines})
    fig.set_tight_layout({'rect': [0, 0, 1, 0.95], 'pad': 1.5, 'h_pad': 1.5})
    fig.patch.set_facecolor(background_color)
    return fig, axes


# UTILS
def save_figure_with_eventual_timestamp(fig, filename, path_results, save_formats, with_timestamp_in_file_name):
    if isinstance(save_formats, str):
        save_formats = [save_formats]
    time_str = ""
    if with_timestamp_in_file_name:
        time_str = datetime.now().strftime("%Y_%m_%d.%H-%M-%S")
    for save_format in save_formats:
        if not with_timestamp_in_file_name:
            save_path = os.path.join(path_results, f"{filename}.{save_format}")
        else:
            save_path = os.path.join(path_results, f"{filename}_{time_str}.{save_format}")
        fig.savefig(save_path, format=save_format)


def plot_similarity_matrix(data, filename=None, background_color=None, size_fig=(10, 8), save_figure=True,
                           path_results=None, save_formats="", with_timestamp_in_file_name=False):
    if isinstance(data, dict):
        key_list = list(data.keys())

        # we plot one graph for each epoch
        n_plots = len(key_list)
        max_n_cols = 5 if n_plots > 6 else 3

        n_cols = min(n_plots, max_n_cols)
        n_lines = math.ceil(n_plots / n_cols)
        print(f"n_lines {n_lines}, n_cols {n_cols}")
        fig, axes = create_and_configure_plot(n_lines, n_cols, size_fig, background_color)

        for index, ax in enumerate(axes.flatten()):
            if index >= n_plots:
                continue
            name = key_list[index]
            data_to_plot = data[name]
            sns.heatmap(data_to_plot, vmin=0, vmax=1, ax=ax, annot=False, xticklabels=50, yticklabels=50)
            ax.set_title(name, fontsize=8)
    else:
        n_lines = 1
        n_cols = 1
        fig, ax = create_and_configure_plot(n_lines, n_cols, size_fig, background_color)
        sns.heatmap(data, vmin=0, vmax=1, ax=ax, annot=False, xticklabels=50, yticklabels=50)
        plt.title(label='Full recording Similarity matrix', fontsize=8)

    if save_figure and path_results:
        save_figure_with_eventual_timestamp(fig, filename, path_results, save_formats, with_timestamp_in_file_name)

    plt.close()
