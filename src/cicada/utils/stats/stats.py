import scipy.stats as sci_stats
import numpy as np
import scikit_posthocs as sp


def calculate_statistics(distribution):
    """Calculate basic statistics for a distribution."""
    length = len(distribution)
    mean = np.nanmean(distribution)
    median = np.nanmedian(distribution)
    sigma = np.nanstd(distribution)
    iqr = sci_stats.iqr(distribution, nan_policy='omit')
    return length, mean, median, sigma, iqr


def print_statistics(distribution, label, verbose):
    """Print statistics for a distribution if verbose is True."""
    length, mean, median, sigma, iqr = calculate_statistics(distribution)
    if verbose:
        print(f"{label}: {length} samples, mean/std: {mean:.3f}/{sigma:.3f}, median/iqr: {median:.2f}/{iqr:.2f}")


def perform_anderson_test(distribution1, distribution2, verbose):
    """Perform Anderson-Darling test and print the result if verbose is True."""
    significance_level = sci_stats.anderson_ksamp([distribution1, distribution2], midrank=True)[2]
    if verbose:
        print(
            f"The null hypothesis that the distributions are the same can be rejected at {significance_level * 100}% significance level")


def perform_ks_test(distribution1, distribution2, pvalues, verbose):
    """Perform KS test and print the result if verbose is True."""
    ks_stat, pvalue = sci_stats.ks_2samp(distribution1, distribution2)
    if verbose:
        result = "not drawn from the same" if pvalue < pvalues else "may be drawn from the same"
        print(f"The distributions are {result} continuous distribution, KS test: D={ks_stat}, p-value={pvalue}")


def perform_epps_singleton_test(distribution1, distribution2, pvalues, verbose):
    """Perform Epps-Singleton test and print the result if verbose is True."""
    stat, pval = sci_stats.epps_singleton_2samp(distribution1, distribution2, t=(0.4, 0.8))
    if verbose:
        result = "not drawn from the same" if pval < pvalues else "may be drawn from the same"
        print(f"The distributions are {result} continuous distribution, Epps-Singleton test: D={stat}, p-value={pval}")


def perform_mann_whitney_test(distribution1, distribution2, verbose):
    """Perform Mann-Whitney test and print the result if verbose is True."""
    stat, pval = sci_stats.mannwhitneyu(distribution1, distribution2, use_continuity=True, alternative='two-sided')
    if verbose:
        print(f"Mann-Whitney sum of rank: U={stat}, p-value={pval}")


def perform_t_tests(distribution1, distribution2, mean_d1, mean_d2, sigma1, sigma2, pvalues, verbose):
    """Perform t-tests and print the results if verbose is True."""
    norm_d1 = (distribution1 - mean_d1) / sigma1
    norm_d2 = (distribution2 - mean_d2) / sigma2

    p_norm1 = sci_stats.shapiro(norm_d1)[1]
    p_norm2 = sci_stats.shapiro(norm_d2)[1]

    if p_norm1 > pvalues and p_norm2 > pvalues:
        if verbose:
            print(f"Both distributions are normally distributed: p-value1={p_norm1}, p-value2={p_norm2}")
        perform_bartlett_and_student_test(distribution1, distribution2, pvalues, verbose)
    else:
        if verbose:
            print(f"At least one distribution is not normally distributed: p-value1={p_norm1}, p-value2={p_norm2}")
        perform_mann_whitney_test(distribution1, distribution2, verbose)


def perform_bartlett_and_student_test(distribution1, distribution2, pvalues, verbose):
    """Perform Bartlett and Student t-tests, printing results if verbose is True."""
    pval_variance = sci_stats.bartlett(distribution1, distribution2)[1]
    if pval_variance > pvalues:
        if verbose:
            print(f"Equality of variances is verified: p-value={pval_variance}")
        t_stat, pval_mean = sci_stats.ttest_ind(distribution1, distribution2, equal_var=True, nan_policy='propagate')
        result = "different" if pval_mean < pvalues else "not different"
        if verbose:
            print(f"The means are {result}, Student test: T={t_stat}, p-value={pval_mean}")
    else:
        if verbose:
            print(f"Equality of variances is not verified: p-value={pval_variance}")
        w_stat, pval_w_mean = sci_stats.ttest_ind(distribution1, distribution2, equal_var=False, nan_policy='propagate')
        result = "different" if pval_w_mean < pvalues else "not different"
        if verbose:
            print(f"The means are {result}, Welch test: T={w_stat}, p-value={pval_w_mean}")


def compare_two_distributions(distribution1, distribution2, pvalues=0.05, verbose=True):
    """Compare two distributions and print results based on various tests."""
    distribution1 = np.array(distribution1)
    distribution2 = np.array(distribution2)

    print_statistics(distribution1, "Distribution1", verbose)
    print_statistics(distribution2, "Distribution2", verbose)

    if len(distribution1) < 25 or len(distribution2) < 25:
        if verbose:
            print("Small sample size: Using Anderson-Darling test")
        perform_anderson_test(distribution1, distribution2, verbose)
    else:
        if verbose:
            print("Large enough sample size: Using KS and Epps-Singleton tests")
        perform_ks_test(distribution1, distribution2, pvalues, verbose)
        perform_epps_singleton_test(distribution1, distribution2, pvalues, verbose)

    if len(distribution1) < 30 or len(distribution2) < 30:
        if verbose:
            print("Small sample size: Using non-parametric comparison of means")
        perform_mann_whitney_test(distribution1, distribution2, verbose)
    else:
        if verbose:
            print("Large enough sample size: Testing normality for parametric comparison of means")
        mean_d1, mean_d2 = np.nanmean(distribution1), np.nanmean(distribution2)
        sigma1, sigma2 = np.nanstd(distribution1), np.nanstd(distribution2)
        perform_t_tests(distribution1, distribution2, mean_d1, mean_d2, sigma1, sigma2, pvalues, verbose)


def test_normality(distributions_list, pvalues):
    """Test normality of distributions and return result."""
    pvalues_normality = [sci_stats.shapiro((dist - np.nanmean(dist)) / np.nanstd(dist))[1] for dist in
                         distributions_list]
    normality = all(pval > pvalues for pval in pvalues_normality)
    return normality, pvalues_normality


def test_homoscedasticity(distributions_list, pvalues):
    """Test homoscedasticity of distributions and return result."""
    pval_variances = sci_stats.bartlett(*distributions_list)[1]
    homoscedasticity = pval_variances > pvalues
    return homoscedasticity, pval_variances


def perform_kruskal_wallis_test(distributions_list, pvalues, verbose, sessions_ids):
    """Perform Kruskal-Wallis test and print results if verbose is True."""
    kw, pvalue = sci_stats.kruskal(*distributions_list, nan_policy='omit')
    if verbose:
        if pvalue >= pvalues:
            print(f"Population medians of all groups are equal: KW-value={kw}, p-value={pvalue}")
        else:
            print(f"At least one group has a different population median: KW-value={kw}, p-value={pvalue}")
            print("Performing Dunn's test with Bonferroni correction")
            pvalues_posthoc = sp.posthoc_dunn(distributions_list, p_adjust='bonferroni')
            print(f"Sessions ids: {sessions_ids}\np-values table:\n{pvalues_posthoc}")


def perform_one_way_anova(distributions_list, pvalues, verbose, sessions_ids):
    """Perform one-way ANOVA test and print results if verbose is True."""
    f, pval = sci_stats.f_oneway(*distributions_list)
    if verbose:
        if pval >= pvalues:
            print(f"Population means of all groups are equal: F-value={f}, p-value={pval}")
        else:
            print(f"At least one group has a different population mean: F-value={f}, p-value={pval}")
            print("Performing Student's test with Holm correction")
            pvalues_posthoc = sp.posthoc_ttest(distributions_list, p_adjust='holm')
            print(f"Sessions ids: {sessions_ids}\np-values table:\n{pvalues_posthoc}")


def multiple_comparison_one_factor_effect(distributions_list, pvalues=0.05, verbose=True, sessions_ids=None):
    """Compare multiple distributions using one-factor effect."""
    n_distributions = len(distributions_list)
    if verbose:
        print(f"Testing hypothesis that these {n_distributions} distributions have the same population mean or median")

    sample_sizes = [len(dist) for dist in distributions_list]
    small_sample = any(size < 30 for size in sample_sizes)

    if small_sample:
        if verbose:
            print("At least one distribution contains less than 30 samples: using Kruskal-Wallis test")
        perform_kruskal_wallis_test(distributions_list, pvalues, verbose, sessions_ids)
    else:
        if verbose:
            print("All distributions contain more than 30 samples: checking one-way ANOVA requirements")

        normality, pvalues_normality = test_normality(distributions_list, pvalues)
        homoscedasticity, pval_variances = test_homoscedasticity(distributions_list, pvalues)

        if verbose:
            print(f"Normality check: {normality}\nHomoscedasticity check: {homoscedasticity}")

        if normality and homoscedasticity:
            if verbose:
                print("Normality and homoscedasticity are verified: using one-way ANOVA")
            perform_one_way_anova(distributions_list, pvalues, verbose, sessions_ids)
        else:
            if verbose:
                print("Normality or homoscedasticity not verified: using Kruskal-Wallis test")
            perform_kruskal_wallis_test(distributions_list, pvalues, verbose, sessions_ids)


def chi_square_uniformity_test(observed_data, binsize):
    bins = np.arange(np.min(observed_data), np.max(observed_data) + binsize, binsize)

    observed_count = np.histogram(observed_data, bins=bins)[0]

    expected_counts = np.ones_like(observed_count) * (len(observed_data) / len(observed_count))

    expected_counts *= observed_count.sum() / expected_counts.sum()

    chi2_stat, p_value = sci_stats.chisquare(f_obs=observed_count, f_exp=expected_counts)

    uniformity = p_value >= 0.05

    return chi2_stat, p_value, uniformity

