import matplotlib.pyplot as plt
import numpy as np
from scipy import signal, stats
import os


def get_sce_threshold(rasterdur, n_surrogates=100, percentile=95, verbose=False):
    [n_cells, n_frames] = rasterdur.shape
    if verbose:
        print(f"Starting to obtain the {n_surrogates} rolled rasters")
    rnd_rasters = np.zeros((n_cells, n_frames, n_surrogates))
    for surrogate in range(n_surrogates):
        for cell in range(n_cells):
            rnd_rasters[cell, :, surrogate] = np.roll(rasterdur[cell, :], np.random.randint(1, n_frames))

    rnd_sum_cells = np.sum(rnd_rasters, axis=0)

    n_values = n_frames * n_surrogates

    rnd_sum_cells_vector = np.reshape(rnd_sum_cells, (n_values,))

    cell_threshold = np.percentile(rnd_sum_cells_vector, percentile)

    if verbose:
        print(f"Threshold obtain for SCE detection with {percentile} percentile: {cell_threshold} cells, "
              f"representing {np.round((cell_threshold / n_cells) * 100, decimals=1)} % of the detected cells")

    return rnd_rasters, cell_threshold


def median_normalization(traces):
    n_cells, n_frames = traces.shape
    norm_traces = np.zeros(traces.shape)
    for i in range(n_cells):
        norm_traces[i, :] = (traces[i, :] - np.nanmedian(traces[i, :])) / np.nanmedian(traces[i, :])
    return norm_traces


def bleaching_correction(traces):
    n_cells, n_frames = traces.shape
    corr_traces = np.zeros(traces.shape)
    for k in range(n_cells):
        p0 = np.polyfit(np.arange(n_frames), traces[k, :], 3)
        corr_traces[k, :] = traces[k, :] / np.polyval(p0, np.arange(n_frames))
    return corr_traces


def savitzky_golay_filt(traces, sr):
    filt_traces = signal.savgol_filter(x=traces, window_length=int(0.5 * sr), polyorder=3, axis=1)
    return filt_traces


def detect_transients(traces, speed=None, use_speed=False, speed_threshold=None, detection_method='Arnaud',
                      window_size=None, min_small_transient_amp=0, imaging_rate=10):
    n_cells, n_frames = traces.shape
    small_transients_raster = np.zeros(traces.shape)

    if use_speed:
        print("Starting detection of small transients using speed threshold")
    else:
        print("Starting detection without speed threshold")

    small_transients_activity = [[] for cell in range(n_cells)]

    for i in range(n_cells):
        trace_tmp = traces[i, :]
        burst_threshold = np.median(trace_tmp) + stats.iqr(trace_tmp) / 2
        n_small_transients = 0

        if detection_method == 'Arnaud':
            for k in range(window_size + 1, n_frames - window_size):
                if use_speed and (speed[k] > speed_threshold):
                    continue

                window_tmp = np.arange(k - window_size, k + window_size)
                median_tmp = np.median(trace_tmp[window_tmp])

                if (np.sum(small_transients_raster[i, np.arange(k - int(imaging_rate), k)]) == 0) \
                        and (median_tmp < burst_threshold) \
                        and ((trace_tmp[k] - median_tmp) > 3 * stats.iqr(trace_tmp[window_tmp])) \
                        and ((trace_tmp[k] - median_tmp) > min_small_transient_amp):
                    small_transients_raster[i, k] = 1
                    small_transients_activity[i].append(trace_tmp[window_tmp])
                    n_small_transients += 1

        else:
            # Detect all transients with 'findpeaks'
            activation_threshold = 3 * stats.iqr(trace_tmp)
            min_peak_prominence = max(activation_threshold, min_small_transient_amp)
            transients_loc, properties = signal.find_peaks(trace_tmp, prominence=min_peak_prominence,
                                                           distance=int(imaging_rate), width=int(imaging_rate / 4))

            # Filter with speed keep only peaks ocuring when speed is below speed threshold
            if use_speed:
                speed_filtered_transients_loc = []
                for transient_loc in transients_loc:
                    if speed[transient_loc] < speed_threshold:
                        speed_filtered_transients_loc.append(transient_loc)
            else:
                speed_filtered_transients_loc = transients_loc

            # Filter on median of window centered on peak: need to be below burst threshold:
            filtered_on_window_median = True
            if filtered_on_window_median:
                filtered_transients_loc = []
                for transient_frame in speed_filtered_transients_loc:

                    local_activity = trace_tmp[np.arange(max(0, transient_frame - window_size),
                                                         min(transient_frame + window_size, n_frames))]
                    if np.nanmedian(local_activity) > burst_threshold:
                        continue
                    else:
                        filtered_transients_loc.append(transient_frame)
            else:
                filtered_transients_loc = speed_filtered_transients_loc

            # Build raster of 'small' transients and keep activity around peaks for plotting
            for transient_frame in filtered_transients_loc:
                small_transients_raster[i, transient_frame] = 1

                if (transient_frame - window_size) < 0 or (transient_frame + window_size) > n_frames:
                    continue
                else:
                    transient_activity = trace_tmp[np.arange(transient_frame - window_size,
                                                             transient_frame + window_size)]
                    small_transients_activity[i].append(transient_activity)

    print("Small transients detection is done")
    print(" ")
    return small_transients_raster, small_transients_activity


def detect_sce(traces, raster, n_synchronous_frames, sce_n_cells_threshold, sce_min_distance):
    n_cells, n_frames = traces.shape
    sum_activity = np.zeros(n_frames - n_synchronous_frames)
    print(f"Sum activity over {n_synchronous_frames} consecutive CI frames")

    for i in range(n_frames - n_synchronous_frames):
        sum_activity[i] = np.sum(np.max(raster[:, np.arange(i, i + n_synchronous_frames)], axis=1))

    print(f"Sum activity is obtained, max is {int(np.max(sum_activity))} cells")

    print(f"Find SCEs: look for peaks above {sce_n_cells_threshold} cells, separated by {sce_min_distance} frames")

    sce_loc = signal.find_peaks(sum_activity, height=sce_n_cells_threshold, distance=sce_min_distance)[0]
    n_sce = len(sce_loc)

    print(f"SCE are detected with {n_sce} SCEs ")

    sce_cells_matrix = np.zeros((n_cells, n_sce))
    for i in range(n_sce):
        sce_cells_matrix[:, i] = np.max(raster[:, np.arange(max((sce_loc[i] - 1), 0),
                                                            min((sce_loc[i] + 2), n_frames))], axis=1)

    return sce_cells_matrix.astype(int), sce_loc, raster


def check_speed_usage(speed, use_speed, speed_threshold):
    if speed is None:
        use_speed = False

    if use_speed:
        if speed_threshold is None:
            speed_threshold = 1  # define below which instantaneous speed (cm/s) we consider mice in rest period
        print(f"{len(np.where(speed <= speed_threshold)[0])} frames with speed below speed threshold "
              f"({speed_threshold} cm/s)")
    else:
        if speed_threshold is not None:
            print(f"Speed threshold is useless if speed is not used")

    return use_speed, speed_threshold


def detect_sce_on_traces(raw_traces, speed=None, use_speed=False, speed_threshold=None, sce_n_cells_threshold=5,
                         transient_method='Arnaud', window_size=4, sce_min_distance=4, n_synchronous_frames=2,
                         small_transient_amp=0, imaging_sampling_rate=10, use_median_norm=True,
                         use_bleaching_correction=False, use_savitzky_golay_filt=False):
    traces = np.copy(raw_traces)

    if use_median_norm is True:
        traces = median_normalization(traces=traces)

    if use_bleaching_correction is True:
        traces = bleaching_correction(traces=traces)

    if use_savitzky_golay_filt is True:
        traces = savitzky_golay_filt(traces=traces, sr=imaging_sampling_rate)

    if use_median_norm or use_bleaching_correction or use_savitzky_golay_filt:
        print(f"Traces normalization done with n traces: {len(traces)}")

    # Detect small transients
    # window_size = 40  # size in frames of the sliding window to detect transients
    # window_size = int(4 * imaging_sampling_rate) # 4 seconds long window as Arnaud was doing with 40 in 10Hz recording
    window_size_frames = int(window_size * imaging_sampling_rate)   # Try a shorter window to better see sometimes it's not a peak

    use_speed, speed_threshold = check_speed_usage(speed=speed, use_speed=use_speed, speed_threshold=speed_threshold)

    small_transients_raster,  small_transients_activity = detect_transients(traces=traces, speed=speed,
                                                                            use_speed=use_speed,
                                                                            speed_threshold=speed_threshold,
                                                                            detection_method=transient_method,
                                                                            window_size=window_size_frames,
                                                                            min_small_transient_amp=small_transient_amp,
                                                                            imaging_rate=imaging_sampling_rate)

    sce_cells_matrix, sce_loc, raster = detect_sce(traces=traces, raster=small_transients_raster,
                                                   n_synchronous_frames=n_synchronous_frames,
                                                   sce_n_cells_threshold=sce_n_cells_threshold,
                                                   sce_min_distance=sce_min_distance)

    return sce_cells_matrix, sce_loc, raster, small_transients_activity


def plot_small_transients(small_transients, window_size, sampling_rate, title, path_results, filename, save_formats):

    square = int(np.ceil(np.sqrt(len(small_transients))))
    fig, axes = plt.subplots(nrows=square, ncols=square, sharex=True, sharey=True,
                             figsize=(1.6 * square, 1.6 * square))
    ax_to_plot = 0
    window_size_frames = int(window_size * sampling_rate)
    time = ((np.arange(0, 2 * window_size_frames, 1)) - window_size_frames) / sampling_rate
    for cell in range(len(small_transients)):
        if small_transients[cell]:
            for transient in small_transients[cell]:
                axes.flatten()[ax_to_plot].plot(time, transient, lw=1, color='grey')
            if len(small_transients[cell]) > 1:
                axes.flatten()[ax_to_plot].plot(time, np.nanmean(small_transients[cell], axis=0), lw=2, color='black')
            axes.flatten()[ax_to_plot].spines[['right', 'top']].set_visible(False)
            # axes.flatten()[ax_to_plot].set_xlabel('t (s)')
            # axes.flatten()[ax_to_plot].set_ylabel('DF/F')
            axes.flatten()[ax_to_plot].set_title(f'Cell {cell}')
            ax_to_plot += 1
        else:
            ax_to_plot += 1
            continue

    fig.suptitle(title)
    fig.tight_layout()

    if path_results is not None:
        for save_format in save_formats:
            fig.savefig(os.path.join(f'{path_results}', f'{filename}.{save_format}'),
                        format=f"{save_format}")
    plt.close('all')
