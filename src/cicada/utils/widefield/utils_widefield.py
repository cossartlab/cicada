import os
import warnings

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from matplotlib.colors import LinearSegmentedColormap, TwoSlopeNorm
from matplotlib.pyplot import get_cmap
from skimage.transform import rescale
from skimage.draw import disk

from cicada.utils.behavior.behavior_analysis_plot_functions import save_fig_and_close
from cicada.utils.misc.array_string_manip import find_nearest


def get_avg_frames_by_type_epoch(session_data, trials, wf_timestamps, widefield_start_in_sec, widefield_stop_in_sec,
                                 keys_ophys_dff0, subtract_baseline=True):
    frames = []
    wf_sampling_rate = 1 / np.median(np.diff(wf_timestamps[0:200]))
    center_frame = int(widefield_start_in_sec * wf_sampling_rate)
    n_frames_post_stim = int(widefield_stop_in_sec * wf_sampling_rate)
    n_frames = center_frame + n_frames_post_stim
    for i, tstamp in enumerate(trials):
        if tstamp < 10:
            continue
        start_time = max(0, tstamp - widefield_start_in_sec)
        frame_start = find_nearest(wf_timestamps, start_time)
        stop_time = min(tstamp + widefield_stop_in_sec, wf_timestamps[-1])
        frame_stop = find_nearest(wf_timestamps, stop_time)
        data = session_data.get_widefield_dff0(keys_ophys_dff0, frame_start, frame_stop)
        try:
            data.shape
        except AttributeError:
            print(trials, wf_timestamps, widefield_start_in_sec, widefield_stop_in_sec)
        if data.shape != (n_frames, 125, 160):
            print(f"Remove trial # {i} doesn't exactly match frame selection size")
            continue
        frames.append(data)

    data_frames = np.array(frames)
    data_frames = np.stack(data_frames, axis=0)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        avg_data = np.nanmean(data_frames, axis=0)
    if subtract_baseline:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            avg_data = avg_data - np.nanmean(avg_data[0:center_frame], axis=0)
    return avg_data, center_frame, n_frames_post_stim


def get_wf_scalebar(scale=1, plot=False, savepath=None):
    # file = r"\\sv-nas1.rcp.epfl.ch\Petersen-Lab\analysis\Robin_Dard\Parameters\Widefield\wf_scalebars\reference_grid_20240314.tif"
    # grid = Image.open(file)
    # im = np.array(grid)
    # im = im.reshape(int(im.shape[0] / 2), 2, int(im.shape[1] / 2), 2).mean(axis=1).mean(axis=2) # like in wf preprocessing
    x = [62 * scale, 167 * scale]
    y = [162 * scale, 152 * scale]
    c = np.sqrt((x[1] - x[0]) ** 2 + (y[0] - y[1]) ** 2)
    # if plot:
    #     fig, ax = plt.subplots()
    #     ax.imshow(rescale(im, scale, anti_aliasing=False))
    #     ax.plot(x, y, c='r')
    #     ax.plot(x, [y[0], y[0]], c='k')
    #     ax.plot([x[1], x[1]], y, c='k')
    #     ax.text(x[0] + int((x[1] - x[0]) / 2), 175*scale, f"{x[1] - x[0]} px")
    #     ax.text(170*scale, 168*scale, f"{np.abs(y[1] - y[0])} px")
    #     ax.text(100*scale, 145*scale, f"{round(c)} px")
    #     ax.text(200*scale, 25*scale, f"{round(c / 6)} px/mm", color="r")
    #     fig.show()
    #     if savepath:
    #         fig.savefig(savepath+rf'\wf_scalebar_scale{scale}.png')
    return round(c / 6)


def get_allen_ccf(bregma=(528, 315),
                  root=r"\\sv-nas1.rcp.epfl.ch\Petersen-Lab\analysis\Robin_Dard\Parameters\Widefield\allen_brain"):
    """Find in utils the AllenSDK file to generate the npy files"""

    ## all images aligned to 240,175 at widefield video alignment, after expanding image, goes to this. Set manually.
    iso_mask = np.load(root + r"\allen_isocortex_tilted_500x640.npy")
    atlas_mask = np.load(root + r"\allen_brain_tilted_500x640.npy")
    bregma_coords = np.load(root + r"\allen_bregma_tilted_500x640.npy")

    displacement_x = int(bregma[0] - np.round(bregma_coords[0] + 20))
    displacement_y = int(bregma[1] - np.round(bregma_coords[1]))

    margin_y = atlas_mask.shape[0] - np.abs(displacement_y)
    margin_x = atlas_mask.shape[1] - np.abs(displacement_x)

    if displacement_y >= 0 and displacement_x >= 0:
        atlas_mask[displacement_y:, displacement_x:] = atlas_mask[:margin_y, :margin_x]
        atlas_mask[:displacement_y, :] *= 0
        atlas_mask[:, :displacement_x] *= 0

        iso_mask[displacement_y:, displacement_x:] = iso_mask[:margin_y, :margin_x]
        iso_mask[:displacement_y, :] *= 0
        iso_mask[:, :displacement_x] *= 0

    elif displacement_y < 0 <= displacement_x:
        atlas_mask[:displacement_y, displacement_x:] = atlas_mask[-margin_y:, :margin_x]
        atlas_mask[displacement_y:, :] *= 0
        atlas_mask[:, :displacement_x] *= 0

        iso_mask[:displacement_y, displacement_x:] = iso_mask[-margin_y:, :margin_x]
        iso_mask[displacement_y:, :] *= 0
        iso_mask[:, :displacement_x] *= 0

    elif displacement_y >= 0 > displacement_x:
        atlas_mask[displacement_y:, :displacement_x] = atlas_mask[:margin_y, -margin_x:]
        atlas_mask[:displacement_y, :] *= 0
        atlas_mask[:, displacement_x:] *= 0

        iso_mask[displacement_y:, :displacement_x] = iso_mask[:margin_y, -margin_x:]
        iso_mask[:displacement_y, :] *= 0
        iso_mask[:, displacement_x:] *= 0

    else:
        atlas_mask[:displacement_y, :displacement_x] = atlas_mask[-margin_y:, -margin_x:]
        atlas_mask[displacement_y:, :] *= 0
        atlas_mask[:, displacement_x:] *= 0

        iso_mask[:displacement_y, :displacement_x] = iso_mask[-margin_y:, -margin_x:]
        iso_mask[displacement_y:, :] *= 0
        iso_mask[:, displacement_x:] *= 0

    return iso_mask, atlas_mask, bregma_coords


def get_colormap(cmap='hotcold'):
    hotcold = ['#aefdff', '#60fdfa', '#2adef6', '#2593ff', '#2d47f9', '#3810dc', '#3d019d',
               '#313131',
               '#97023d', '#d90d39', '#f8432d', '#ff8e25', '#f7da29', '#fafd5b', '#fffda9']

    cyanmagenta = ['#00FFFF', '#FFFFFF', '#FF00FF']

    if cmap == 'cyanmagenta':
        cmap = LinearSegmentedColormap.from_list("Custom", cyanmagenta)

    elif cmap == 'whitemagenta':
        cmap = LinearSegmentedColormap.from_list("Custom", ['#FFFFFF', '#FF00FF'])

    elif cmap == 'hotcold':
        cmap = LinearSegmentedColormap.from_list("Custom", hotcold)

    elif cmap == 'grays':
        cmap = get_cmap('Greys')

    elif cmap == 'viridis':
        cmap = get_cmap('viridis')

    elif cmap == 'blues':
        cmap = get_cmap('Blues')

    elif cmap == 'magma':
        cmap = get_cmap('magma')

    else:
        cmap = get_cmap(cmap)

    cmap.set_bad(color='k', alpha=0.1)

    return cmap


def plot_wf_timecourses(avg_data, center_frame, n_frames_post_stim, n_frames_averaged, title, save_path, formats,
                        vmin=-0.005, vmax=0.035):
    n_frames = n_frames_post_stim + center_frame
    n_cols = int(np.ceil(np.sqrt(n_frames)))
    n_rows = int(np.ceil(n_frames / n_cols))

    fig, ax = plt.subplots(n_rows, n_cols, figsize=(15, 15))
    fig.suptitle(title)

    for i, frame in enumerate(range(center_frame, center_frame + n_frames_post_stim)):
        if frame + n_frames_averaged > center_frame + n_frames_post_stim:
            break
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            frame_mean = np.nanmean(avg_data[frame:frame + n_frames_averaged], axis=0)
        plot_wf_single_frame(frame=frame_mean,
                             title=f'{center_frame}-{center_frame + n_frames_averaged}',
                             figure=fig, ax_to_plot=ax.flat[i], suptitle=' ',
                             saving_path=save_path, save_formats=formats,
                             colormap='hotcold', vmin=vmin, vmax=vmax, halfrange=0.04, cbar_shrink=1.0)

    save_fig_and_close(results_path=save_path, save_formats=formats, figure=fig, figname='')


def plot_wf_single_frame(frame, title, figure, ax_to_plot, suptitle, saving_path, save_formats, colormap='hotcold',
                         vmin=-0.005, vmax=0.02, halfrange=0.04, cbar_shrink=1.0, separated_plots=True):

    subfig, ax = plt.subplots(1, 1, figsize=(4, 4), frameon=False)

    bregma = (488, 290)
    scale = 4
    scalebar = get_wf_scalebar(scale=scale)
    iso_mask, atlas_mask, allen_bregma = get_allen_ccf(bregma)
    cmap = get_colormap(colormap)
    cmap.set_bad(color="white")

    single_frame = np.rot90(rescale(frame, scale, anti_aliasing=False))
    single_frame = np.pad(single_frame, [(0, 650 - single_frame.shape[0]), (0, 510 - single_frame.shape[1])],
                          mode='constant', constant_values=np.nan)

    mask = np.pad(iso_mask, [(0, 650 - iso_mask.shape[0]), (0, 510 - iso_mask.shape[1])], mode='constant',
                  constant_values=np.nan)
    single_frame = np.where(mask > 0, single_frame, np.nan)

    if colormap == 'hotcold':
        cmap = get_colormap('hotcold')
        norm = TwoSlopeNorm(vmin=vmin, vcenter=0, vmax=vmax)
        im = ax_to_plot.imshow(single_frame, norm=norm, cmap=cmap)
        im_2 = ax.imshow(single_frame, norm=norm, cmap=cmap)
        cmap.set_bad(color="white")
    else:
        norm = colors.CenteredNorm(halfrange=halfrange)
        im = ax_to_plot.imshow(single_frame, cmap="seismic", norm=norm)
        im_2 = ax.imshow(single_frame, cmap="seismic", norm=norm)

    # Put the image on main figure
    ax_to_plot.contour(atlas_mask, levels=np.unique(atlas_mask), colors='gray',
                       linewidths=1)
    ax_to_plot.contour(iso_mask, levels=np.unique(np.round(iso_mask)), colors='black',
                       linewidths=2, zorder=2)
    ax_to_plot.scatter(bregma[0], bregma[1], marker='+', c='k', s=100, linewidths=2,
                       zorder=3)
    ax_to_plot.hlines(25, 25, 25 + scalebar * 3, linewidth=2, colors='k')
    ax_to_plot.text(50, 100, "3 mm", size=10)
    ax_to_plot.set_title(title)
    figure.colorbar(im, ax=ax_to_plot, location='right', shrink=cbar_shrink)

    # Put on subfig for individual image
    if separated_plots:
        subfig.suptitle(suptitle)
        ax.set_title(title)
        ax.contour(atlas_mask, levels=np.unique(atlas_mask), colors='gray', linewidths=1)
        ax.contour(iso_mask, levels=np.unique(np.round(iso_mask)), colors='black', linewidths=2, zorder=2)
        ax.scatter(bregma[0], bregma[1], marker='+', c='k', s=100, linewidths=2, zorder=3)
        ax.hlines(25, 25, 25 + scalebar * 3, linewidth=2, colors='k')
        ax.text(50, 100, "3 mm", size=10)
        subfig.colorbar(im_2, ax=ax, location='right', shrink=cbar_shrink)
        save_fig_and_close(results_path=saving_path, save_formats=save_formats, figure=subfig,
                           figname=f'{suptitle}_{title}' if suptitle != ' ' else f'{title}')

    plt.close()


def plot_wf_avg(avg_data, output_path, n_frames_post_stim, n_frames_averaged, key, center_frame,
                figname, save_formats, subdir, c_scale=(-0.005, 0.02), halfrange=0.04, colormap='hotcold'):
    path = os.path.join(output_path, f"{subdir}")
    if not os.path.exists(path):
        os.makedirs(path)

    cutlet_idx = 0
    n_cols = int(np.ceil(n_frames_post_stim / n_frames_averaged))
    fig, axes = plt.subplots(1, n_cols,
                             figsize=(5 * n_cols, 5),
                             frameon=False)
    fig.suptitle(f'{key}')
    for start in range(center_frame, center_frame + n_frames_post_stim, n_frames_averaged):
        if start + n_frames_averaged > center_frame + n_frames_post_stim:
            break
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            frame_mean = np.nanmean(avg_data[start:start + n_frames_averaged], axis=0)
        plot_wf_single_frame(frame_mean,
                             title=f'{start - center_frame}-{start - center_frame + n_frames_averaged}',
                             figure=fig,
                             ax_to_plot=axes[cutlet_idx],
                             suptitle=key,
                             colormap=colormap,
                             vmin=c_scale[0],
                             vmax=c_scale[1],
                             halfrange=halfrange,
                             cbar_shrink=0.75,
                             saving_path=path,
                             save_formats=save_formats)
        cutlet_idx += 1
    fig.tight_layout()

    save_fig_and_close(results_path=path, save_formats=save_formats, figure=fig,
                       figname=f'{figname}_wf_avg')


def center_rrs_on_events(traces, traces_ts, event_ts, stim_ts, time_range, sampling_rate, align_to,
                         subtract_baseline=False):

    time_range = (int(np.ceil(sampling_rate * time_range[0])),
                  int(np.floor(sampling_rate * time_range[1])))

    event_frames = []
    for event in event_ts:
        event_frames.append(find_nearest(traces_ts, event))

    n_cells = traces.shape[0]
    n_tp = int(time_range[0] + time_range[1] + 1)
    activity_aligned = np.zeros((n_cells, len(event_frames), n_tp)) * np.nan
    trial_baseline = np.zeros((n_cells, len(event_frames))) * np.nan

    for idx, frame in enumerate(event_frames):
        if frame + time_range[1] + 1 > traces.shape[1]:  # Remove event if too close to the end
            continue
        if frame - time_range[0] < 0:  # Remove event if too close to the start
            continue
        # Get aligned activity
        activity_aligned[:, idx] = traces[:, frame - time_range[0]: frame + time_range[1] + 1]
        # Retrieve sensory baseline if align to lick
        if (align_to == 'Lick') and (stim_ts is not None):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                trial_baseline[:, idx] = np.nanmean(traces[:, find_nearest(traces_ts, stim_ts[idx]) - 10:
                                                    find_nearest(traces_ts, stim_ts[idx])], axis=1)

    if subtract_baseline:
        if align_to == 'Lick':
            for idx in range(activity_aligned.shape[1]):
                activity_aligned[:, idx, :] = activity_aligned[:, idx, :] - trial_baseline[:, idx]
        else:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                activity_aligned = activity_aligned - np.nanmean(activity_aligned[:, :, 0:time_range[0]], axis=2,
                                                                 keepdims=True)

    return activity_aligned


def select_activity_around_events_pd(activity, activity_ts, rois, events, stims, time_range, sampling_rate,
                                     subtract_baseline, align_to, **metadata):
    dfs = []
    activity_aligned = center_rrs_on_events(activity, activity_ts,
                                            events, stims, time_range,
                                            sampling_rate, align_to, subtract_baseline)
    n_cells, n_events, n_t = activity_aligned.shape

    time_stamps_vect = np.linspace(-time_range[0], time_range[1], n_t)
    time_stamps_vect = np.tile(time_stamps_vect, n_cells * n_events)

    event_vect = np.tile(np.repeat(np.arange(n_events), n_t), n_cells)
    rois_vect = np.repeat(rois, n_events * n_t)
    activity_reshaped = activity_aligned.flatten()

    df = dict({'activity': activity_reshaped, 'time': time_stamps_vect, 'event': event_vect, 'roi': rois_vect})
    df = pd.DataFrame.from_dict(df)

    # Add session metadata.
    for key, val in metadata.items():
        df[key] = val

    dfs.append(df)

    return pd.concat(dfs, ignore_index=True)


def update_psth_dfs(activity, cell_types_to_do, cell_type_dict, activity_ts, trials_kept, stim_times,
                    time_range, sampling_rate, align_to, subtract_baseline,
                    mouse_id, session_id, behavior_type, behavior_day):
    dfs = []
    if cell_type_dict:
        for cell_type, rois in cell_type_dict.items():
            if cell_type in cell_types_to_do:
                activity_filtered = activity[rois]
                df = select_activity_around_events_pd(activity_filtered, activity_ts, rois, trials_kept, stim_times,
                                                      time_range, sampling_rate, subtract_baseline, align_to,
                                                      mouse_id=mouse_id, session_id=session_id,
                                                      behavior_type=behavior_type,
                                                      behavior_day=behavior_day, cell_type=cell_type)
                dfs.append(df)
    else:
        rois = np.arange(activity.shape[0])
        df = select_activity_around_events_pd(activity, activity_ts, rois, trials_kept, stim_times, time_range,
                                              sampling_rate, subtract_baseline, align_to, mouse_id=mouse_id,
                                              session_id=session_id, behavior_type=behavior_type,
                                              behavior_day=behavior_day, cell_type='na')
        dfs.append(df)
    return pd.concat(dfs, ignore_index=True)


def trial_type_interpreter(trial_type):
    lick_flag = 1 if 'hit' in trial_type or 'false_alarm' in trial_type else 0
    if 'whisker' in trial_type:
        stim_type_string = 'whisker_stim'
    elif 'auditory' in trial_type:
        stim_type_string = 'auditory_stim'
    else:
        stim_type_string = 'no_stim'
    return lick_flag, stim_type_string


def trial_type_baseline(trial_type, trial_table, rrs_ts, keys_ophys_dff0, quiet_window_mode,
                        general_img_dict_without_trial_type,
                        general_img_dict, general_img_transition_time_no_type_dict, general_img_transition_time_dict,
                        session_data, mouse_id, session_id, epoch, epoch_starts, shape_x, shape_y):
    context_replace_dict = {'Rewarded': 'rewarded', 'Non-Rewarded': 'non-rewarded'}
    trial_table = trial_table.replace({'context': context_replace_dict})
    lick_flag, stim_type_string = trial_type_interpreter(trial_type)
    trial_type_table = trial_table.loc[(trial_table[stim_type_string] == 1) & (trial_table.lick_flag == lick_flag) &
                                       (trial_table.context == epoch)]
    # Get quiet windows
    avg_data_list = []
    time_in_context_list = []
    for trial in range(len(trial_type_table)):
        quiet_start = trial_type_table.iloc[trial].abort_window_start_time
        stim_time = trial_type_table.iloc[trial].start_time
        if quiet_window_mode == 'last 2s':
            start_frame = find_nearest(rrs_ts, stim_time - 2)
        elif quiet_window_mode == 'last 0.5s':
            start_frame = find_nearest(rrs_ts, stim_time - 0.5)
        else:
            start_frame = find_nearest(rrs_ts, quiet_start)
        end_frame = find_nearest(rrs_ts, stim_time)

        filtered_epoch_ts = [t for t in epoch_starts if t < stim_time]
        closest_below = max(filtered_epoch_ts) if filtered_epoch_ts else np.nan
        time_in_context_list.append(stim_time - closest_below)

        data = session_data.get_widefield_dff0(keys_ophys_dff0, start_frame, end_frame)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            avg_data = np.nanmean(data, axis=0)
        avg_data_list.append(avg_data)

    if len(avg_data_list) == 0:
        print(f'No data to stack for {trial_type}, continue')
        general_img_dict_without_trial_type[mouse_id][session_id][epoch].append(np.full((1, shape_x, shape_y), np.nan))
        general_img_dict[mouse_id][session_id][epoch][trial_type].append(np.full((1, shape_x, shape_y), np.nan))
        return

    avg_img = np.stack(avg_data_list)
    general_img_dict_without_trial_type[mouse_id][session_id][epoch].append(avg_img)
    general_img_dict[mouse_id][session_id][epoch][trial_type].append(avg_img)

    general_img_transition_time_no_type_dict[mouse_id][session_id][epoch].append(time_in_context_list)
    general_img_transition_time_dict[mouse_id][session_id][epoch][trial_type].append(time_in_context_list)


def get_session_average_wf_baseline_images(trial_table, session_data, wf_ts, rrs_keys, quiet_window_mode):
    avg_data_list = []
    for trial in range(len(trial_table)):
        quiet_start = trial_table.iloc[trial].abort_window_start_time
        stim_time = trial_table.iloc[trial].start_time
        if quiet_window_mode == 'last 2s':
            start_frame = find_nearest(wf_ts, stim_time - 2)
        elif quiet_window_mode == 'last 0.5s':
            start_frame = find_nearest(wf_ts, stim_time - 0.5)
        else:
            start_frame = find_nearest(wf_ts, quiet_start)
        end_frame = find_nearest(wf_ts, stim_time)
        data = session_data.get_widefield_dff0(rrs_keys, start_frame, end_frame)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            avg_data = np.nanmean(data, axis=0)
        avg_data_list.append(avg_data)
    avg_data_list = np.stack(avg_data_list)

    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        session_avg = np.nanmean(avg_data_list, axis=0)

    return session_avg


def average_images_from_dict(image_dict):
    images = []
    for key, value in image_dict.items():
        if isinstance(value, dict):
            images.append(average_images_from_dict(value))
        elif isinstance(value, list):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                images.append(np.nanmean(np.stack(value), axis=0))
        else:
            images.append(value)

    images = np.stack(images)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        return np.nanmean(images, axis=0)


def map_coords_to_image(im, coords):

    total_im = []
    for i, c in enumerate(coords):
        im_dict ={}
        im_dict['x'] = c[1]
        im_dict['y'] = c[0]
        im_dict['dff0'] = im[i]
        total_im += [im_dict]
    return pd.DataFrame(total_im)


def generate_reduced_image_df(image, coords):

    total_df = []
    for i in range(image.shape[0]):
        df = map_coords_to_image(image[i], coords=coords)
        df['frame'] = i
        total_df += [df]
    return pd.concat(total_df)


def reduce_im_dimensions(image):
    y = np.linspace(-5, 0, 6, endpoint=True).astype(int) - 0.5
    x = np.linspace(-2, 4, 7, endpoint=True).astype(int) - 0.5
    xn, yn = np.meshgrid(x, y)
    bregma = (88, 120)
    scale = 1
    scalebar = 18
    wf_x = bregma[0] - xn * scalebar
    wf_y = bregma[1] + yn * scalebar

    im_downsampled = []
    for x, y in zip(np.flip(wf_x).flatten().astype(int), wf_y.flatten().astype(int)):
        rr, cc = disk((x, y), scalebar/2)
        im_downsampled += [np.nanmean(image[:, cc, rr], axis=1)]

    return np.stack(im_downsampled, axis=1), list(zip(np.flip(xn).flatten(), yn.flatten()))
