def get_stability_among_cell_assemblies(assemblies_1, assemblies_2, divide_by_total_of_both=True):
    """

    Args:
        assemblies_1: list of list of int reprensenting the index of a cell
        assemblies_2:
        divide_by_total_of_both: if True, we divide the number of cell in common by the total amount
        of different cells represented by the two assemblies that has been compared, otherwise we divide it
        just by the number of cell in assembly_1

    Returns: list of the same size as assembly_1, of integers representing the percentage of cells in each
    assembly that are part of a same assembly in assemblies_2

    """

    perc_list = list()
    for ass_1 in assemblies_1:
        if len(ass_1) == 0:
            continue
        max_perc = 0
        for ass_2 in assemblies_2:
            # easiest way would be to use set() and intersection, but as 2 channels could have the same name
            # we want to have 2 instances different, even so we won't know for sure if that's the same
            n_in_common = len(list(set(ass_1).intersection(ass_2)))
            all_cells = []
            all_cells.extend(ass_1)
            all_cells.extend(ass_2)
            n_different_channels = len(set(all_cells))
            #
            if divide_by_total_of_both:
                perc = (n_in_common / n_different_channels) * 100
            else:
                perc = (n_in_common / len(ass_1)) * 100
            max_perc = max(max_perc, perc)
        perc_list.append(max_perc)

    return perc_list