import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os

from cicada.utils.behavior.behavior_analysis_utils import plot_with_point_and_strip


########################################
# Utils
########################################

def ax_set(ax, ylim=[-0.1, 1.05], xlabel='Day', ylabel='Lick probability'):
    """
    Sets the limits and labels of the axes, and removes the top and right spines.

    Args:
        ax: The matplotlib axes to modify.
        ylim: The y-axis limits (default is [-0.1, 1.05]).
        xlabel: The label for the x-axis (default is 'Day').
        ylabel: The label for the y-axis (default is 'Lick probability').
    """
    ax.set_ylim(ylim)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    sns.despine()


def set_figure_background_axis_colors(figure, ax, background_color, labels_color):
    ax.set_facecolor(background_color)
    figure.patch.set_facecolor(background_color)
    ax.yaxis.label.set_color(labels_color)
    ax.xaxis.label.set_color(labels_color)
    ax.spines['left'].set_color(labels_color)
    ax.spines['bottom'].set_color(background_color)
    ax.tick_params(axis='y', colors=labels_color)
    ax.tick_params(axis='x', colors=labels_color)


def save_fig_and_close(results_path, save_formats, figure, figname):
    """
    Saves a figure in multiple formats and closes the figure.

    Args:
        results_path: Base path where figures will be saved.
        save_formats: List of file formats (e.g., ['png', 'pdf']) to save the figure.
        figure: The figure to save.
        figname: The base name for the saved figure files.
    """
    figure.tight_layout()
    if not os.path.exists(results_path):
        os.makedirs(results_path)
    for save_format in save_formats:
        figure.savefig(os.path.join(results_path, f'{figname}.{save_format}'),
                       format=f"{save_format}")
    plt.close('all')

##########################
# main functions
############################


def categorical_context_lineplot(data, hue, style, palette, mouse_id, save_formats, figure_size, dpi, markers,
                                 bkgrd_col, label_col, saving_path, figname):
    figure, ax0 = plt.subplots(1, 1, figsize=figure_size, dpi=dpi)

    for element in (('outcome_n', 'catch_palette'), ('outcome_a', 'aud_palette'), ('outcome_w', 'wh_palette')):
        sns.lineplot(x='day', y=element[0], hue=hue, style=style, data=data, palette=palette[element[1]],
                     ax=ax0, markers=markers, marker="o")

    ax0.set_title(f"Mouse {figname[0: 6]} across context days")
    ax0.get_legend().set_visible(False)
    ax_set(ax0, ylim=[-0.1, 1.05], xlabel='Day', ylabel='Lick probability')

    # Set the colors
    set_figure_background_axis_colors(figure, ax0, background_color=bkgrd_col, labels_color=label_col)

    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def categorical_context_days_avg(data, x_name, hue, palette, mouse_id, save_formats, figure_size, dpi,
                                 saving_path, figname):
    figure, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=figure_size, dpi=dpi)

    for element in (('outcome_n', ax0, 'catch_palette'), ('outcome_a', ax1, 'aud_palette'),
                    ('outcome_w', ax2, 'wh_palette')):
        plot_with_point_and_strip(data=data, x_name=x_name, y_name=element[0], hue=hue, legend=False,
                                  palette=palette, ax=element[1], palette_key=element[2], link_mice=False)
    n_days = len(np.unique(data.day.values[:]))
    subtitles = ['catch trials', 'auditory trials', 'whisker trials']
    for ind, ax in enumerate([ax0, ax1, ax2]):
        ax.set_title(f"{subtitles[ind]}")
        ax_set(ax, ylim=[0, 1.05], xlabel='Context', ylabel='Lick probability')

    figure.suptitle(f"Average performance across {n_days} days for mouse {mouse_id}")
    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def categorical_context_boxplot(data, hue, palette, mouse_id, save_formats, figsize, dpi, saving_path, figname):
    figure, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=figsize, dpi=dpi)

    for element in (('hr_n', 'catch_palette', ax0), ('hr_a', 'aud_palette', ax1), ('hr_w', 'wh_palette', ax2)):
        sns.boxenplot(x='day', y=element[0], hue=hue, data=data, palette=palette[element[1]], ax=element[2])

    for ax in [ax0, ax1, ax2]:
        ax_set(ax, ylim=[0, 1.05], xlabel='Day', ylabel='Lick probability')

    plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)

    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def categorical_context_stripplot(data, hue, palette, mouse_id, save_formats, figsize, dpi, saving_path, figname):
    figure, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=figsize, dpi=dpi)

    for element in (('hr_n', 'catch_palette', ax0), ('hr_a', 'aud_palette', ax1), ('hr_w', 'wh_palette', ax2)):
        sns.stripplot(x='day', y=element[0], hue=hue, data=data, palette=palette[element[1]], dodge=True,
                      jitter=0.2, ax=element[2])

    for ax in [ax0, ax1, ax2]:
        ax_set(ax, ylim=[0, 1.05], xlabel='Day', ylabel='Lick probability')

    plt.legend(bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)

    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def categorical_context_pointplot(data, hue, palette, mouse_id, save_formats, figsize, dpi, saving_path, figname,
                                  show_single_bloc):
    figure, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=figsize, dpi=dpi)

    for element in (('hr_n', data.hr_n.values, 'catch_palette', ax0), ('hr_a', data.hr_a.values, 'aud_palette', ax1),
                    ('hr_w', data.hr_w.values, 'wh_palette', ax2)):
        if element[0] in list(data.columns) and (not np.isnan(element[1][:]).all()):
            sns.pointplot(x='day', y=element[0], hue=hue, data=data, palette=palette[element[2]], dodge=True,
                          estimator=np.mean, errorbar=('ci', 95), n_boot=1000, ax=element[3])
        if show_single_bloc:
            if element[0] in list(data.columns) and (not np.isnan(element[1][:]).all()):
                sns.stripplot(x='day', y=element[0], hue=hue, data=data, palette=palette[element[2]], dodge=False,
                              marker='o', alpha=.5, ax=element[3])

    subtitles = ['catch trials', 'auditory trials', 'whisker trials']
    for ind, ax in enumerate([ax0, ax1, ax2]):
        ax.set_title(f"{subtitles[ind]}")
        ax_set(ax, ylim=[0, 1.05], xlabel='Day', ylabel='Lick probability')
        ax.get_legend().set_visible(False)

    figure.suptitle(f"Mouse {mouse_id} across context days")
    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def categorical_context_opto_pointplot(data, hue, palette, mouse_id, save_formats, figsize, dpi, saving_path, figname):
    figure, (ax0, ax1, ax2) = plt.subplots(1, 3, figsize=figsize, dpi=dpi)

    for element in (('hr_n', 'catch_palette', ax0), ('hr_a', 'aud_palette', ax1), ('hr_w', 'wh_palette', ax2)):
        sns.pointplot(x='day', y=element[0], hue=hue, data=data.loc[data['opto_stim'] == 0],
                      palette=palette[element[1]], dodge=True, estimator=np.mean,
                      errorbar=('ci', 95), n_boot=1000, ax=element[2])
        sns.pointplot(x='day', y=element[0], hue=hue, data=data.loc[data['opto_stim'] == 1],
                      palette=palette[element[1]], dodge=True, estimator=np.mean, errorbar=('ci', 95),
                      n_boot=1000, ax=element[2], markers='*', linestyles='dashed')

    for ax in [ax0, ax1, ax2]:
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(title=f'{mouse_id}', handles=handles,
                  labels=['Non Rewarded', 'Rewarded', 'Non Rewarded opto', 'Rewarded opto'])
        ax_set(ax, ylim=[0, 1.05], xlabel='Day', ylabel='Lick probability')
        plt.legend(loc='lower left', borderaxespad=0)

    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)


def plot_difference_between_context(data, palette, mouse_id, save_formats, saving_path, figure_size):
    figure, ax0 = plt.subplots(1, 1, figsize=figure_size)

    for elem in (('hr_n_diff', 5), ('hr_a_diff', 0), ('hr_w_diff', 2)):
        sns.pointplot(x='day', y=elem[0], data=data, color=palette[elem[1]], ax=ax0, markers='o')

    ax0.set_title(f"{mouse_id}")
    ax_set(ax0, ylim=[-0.2, 1.05], xlabel='Day', ylabel='Lick probability')

    figname = f"{mouse_id}_context_difference"
    results_path = os.path.join(saving_path, f'{mouse_id}')
    save_fig_and_close(results_path, save_formats, figure, figname)
