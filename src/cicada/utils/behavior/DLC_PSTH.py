import numpy as np
import pandas as pd
import seaborn as sns

from cicada.utils.behavior.behavior_analysis_plot_functions import save_fig_and_close


def filter_events_based_on_epochs(events_ts, epochs, verbose):
    event_in_epoch = []
    for event in events_ts:
        n_epochs = epochs.shape[1]
        keep_event = False
        for sub_epoch in range(n_epochs):
            main_epoch_start = epochs[0, sub_epoch]
            main_epoch_stop = epochs[1, sub_epoch]
            if main_epoch_start < event < main_epoch_stop:
                keep_event = True
                break
        event_in_epoch.append(keep_event)

    events_filtered = np.array(events_ts)[event_in_epoch]

    return events_filtered


def plot_psth_dlc(data, x='time', y='mean_trace', col='bodypart', row='session_id', kind='line', hue=None, style=None,
                  color_plot='blue', line_width=2, background_color="black", color_ticks="white",
                  axes_label_color="white",
                  axes_border_color="white", color_v_line="white", figsize=(15, 10), ticks_labels_size=5,
                  axis_label_size=30, axis_label_pad=20, legend_police_size=10, ticks_length=6, ticks_width=2,
                  y_lim_values=None, save_path=None, save_formats="pdf", file_name="psth_dlc"):
    fig = sns.relplot(data=data, x=x, y=y, col=col, row=row, kind=kind, hue=hue, style=style, errorbar='sd')

    for ax in fig.axes.flatten():
        ax.axvline(color='red', linestyle='--')

    fig.set_titles(col_template="{col_name}", row_template="{row_name}")

    save_fig_and_close(save_path, save_formats, fig, file_name)


def create_trial_df(session_data, body_part, start_frame_index, stop_frame_index, baseline_frame_duration,
                    psth_start_in_sec, psth_stop_in_sec, events_name, epoch):
    bodypart_dataframe = pd.DataFrame()
    behavior_movie_data = session_data.get_behavioral_time_series_data(body_part)
    behavior_movie_data_event = behavior_movie_data[start_frame_index:stop_frame_index]
    behavior_movie_data_event -= np.nanmean(behavior_movie_data_event[:baseline_frame_duration])
    bodypart_dataframe['behavior_movie_data_event'] = behavior_movie_data_event
    bodypart_dataframe['mouse_id'] = session_data.subject_id
    bodypart_dataframe['session_id'] = session_data.session_id
    bodypart_dataframe['bodypart'] = body_part
    bodypart_dataframe['time'] = np.linspace(-psth_start_in_sec, psth_stop_in_sec,
                                             len(behavior_movie_data_event))
    bodypart_dataframe['trial_name'] = events_name
    bodypart_dataframe['epoch'] = epoch

    return bodypart_dataframe
