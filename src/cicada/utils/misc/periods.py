import numpy as np

from cicada.utils.misc.array_string_manip import find_nearest


def get_continous_time_periods(binary_array):
    """
    Takes a binary array and returns a list of tuples representing the first and last position (inclusive)
    of continuous positive periods.

    Args:
        binary_array (np.ndarray): A binary array containing only 0s and 1s.

    Returns:
        List[Tuple[int, int]]: A list of tuples representing continuous periods of 1s.
    """
    # Ensure the array is binary (0 and 1)
    binary_array = np.clip(np.asarray(binary_array, dtype=np.int8), 0, 1)

    # Identify the start (1) and end (-1) of continuous periods of 1s
    d_times = np.diff(binary_array)
    starts = np.where(d_times == 1)[0] + 1
    ends = np.where(d_times == -1)[0] + 1

    # Handle edge cases where the array starts or ends with a continuous period of 1s
    if binary_array[0] == 1:
        starts = np.insert(starts, 0, 0)
    if binary_array[-1] == 1:
        ends = np.append(ends, len(binary_array))

    # Pair starts and ends into tuples
    periods = list(zip(starts, ends - 1))

    return periods


def from_timestamps_to_frame_epochs(time_stamps_array, frames_timestamps, as_list):
    """

    Args:
        time_stamps_array: 2d array (2, n_points)
        frames_timestamps: 1d array of float of length n_frames, containing the timestamps of the frames
        as_list: if True, return a list of tuples of 2 int, or a 2d of (2, n_frames) dimensions

    Returns:

    """
    first_frame_ts = frames_timestamps[0]
    last_frame_ts = frames_timestamps[-1]

    intervals = []

    for index_ts in range(time_stamps_array.shape[1]):
        first_ts = time_stamps_array[0, index_ts]
        last_ts = time_stamps_array[1, index_ts]

        if (first_ts < first_frame_ts) or (last_ts < first_frame_ts):
            continue

        if last_ts > last_frame_ts:
            continue

        first_frame = find_nearest(frames_timestamps, value=first_ts)
        last_frame = find_nearest(frames_timestamps, value=last_ts)

        if as_list:
            intervals.append((first_frame, last_frame))
        else:
            intervals.append([first_frame, last_frame])

    if not as_list:
        intervals = np.array(intervals).T

    return intervals


def from_timestamps_array_to_list(time_stamps_array, frames_timestamps):
    """

    Args:
        time_stamps_array: 2d array (2, n_points)
        frames_timestamps: 1d array of float of length n_frames, containing the timestamps of the frames,
        used to keep only the epochs in this range

    Returns: a list of tuples of 2 float, or a 2d of (2, n_frames) dimensions

    """
    first_frame_ts = frames_timestamps[0]
    last_frame_ts = frames_timestamps[-1]

    intervals_list = []

    for index_ts in range(time_stamps_array.shape[1]):
        first_ts = time_stamps_array[0, index_ts]
        last_ts = time_stamps_array[1, index_ts]

        # keeping only the one in range of the frame timestamps
        if (first_ts < first_frame_ts) or (last_ts < first_frame_ts):
            continue

        if last_ts > last_frame_ts:
            continue

        intervals_list.append((first_ts, last_ts))

    return intervals_list


def get_yang_frames(total_frames=12500, yin_frames=None):
    """
    :param total_frames: total number of frames
    :param yin_frames: a list of tuples with start and end of yin frame periods
    :return: bool_yang_frames: True if Yang, False if Yin
    """
    # Boolean variable yang frames to output
    bool_yang_frames = np.ones((total_frames, ), dtype=bool)

    for start_period, end_period in yin_frames:
        bool_yang_frames[start_period:end_period + 1] = False

    yang_frames_list = np.where(bool_yang_frames)[0]

    return bool_yang_frames, yang_frames_list


def fill_gaps_in_continuous_periods(boolean_serie, max_gap_size):
    """

    :param boolean_serie:
    :param max_gap_size:
    :return:
    """
    filled_boolean_serie = boolean_serie
    continuous_periods = get_continous_time_periods(boolean_serie)
    if not continuous_periods:
        print(f"No continuous time periods found, return unchanged serie")
        return boolean_serie

    for epoch_start, epoch_end in zip(continuous_periods[1:], continuous_periods[:-1]):
        gap_size = epoch_start[0] - epoch_end[1]
        if gap_size <= max_gap_size:
            filled_boolean_serie[epoch_end[1]:epoch_start[0]] = True

    return filled_boolean_serie
