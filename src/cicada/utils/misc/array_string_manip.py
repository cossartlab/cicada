import collections
import math
import re

import numpy as np


def find_nearest(array, value, is_sorted=True):
    """
    Return the index of the nearest content in array of value.
    from https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
    return -1 or len(array) if the value is out of range for sorted array
    Args:
        array:
        value:
        is_sorted:

    Returns:

    """
    if len(array) == 0:
        return -1

    if is_sorted:
        if value < array[0]:
            return -1
        elif value > array[-1]:
            return len(array)
        idx = np.searchsorted(array, value, side="left")
        if idx > 0 and (idx == len(array) or math.fabs(value - array[idx - 1]) < math.fabs(value - array[idx])):
            return idx - 1
        else:
            return idx
    else:
        array = np.asarray(array)
        idx = (np.abs(array - value)).idxmin()
        return idx


def validate_indices_in_string_format(str_indices):
    """
    Take a string representing a serie of indices (int) and make sure the format is correct,
    the string then could be used as an argument of extract_indices_from_string()
    There should be only '-' and numbers
    Ex: "1-3 5 10-20-30 6" is in a correct format
    "1 - 5 20.6 toto" is not correct
    Args:
        str_indices: (str)

    Returns: either False or a string indicating why is it False, if the format is correct return True

    """

    if not isinstance(str_indices, str):
        return "Not a string"

    str_indices = str_indices.strip()

    if len(str_indices) == 0:
        return "Empty string"

    # we check them one by one
    n_char = len(str_indices)
    str_indices_split = str_indices.split()
    for str_indice in str_indices_split:
        if str_indice.count("-") > 1:
            return "There shouldn't be more than one '-'"
        for index, character in enumerate(str_indice):
            if character == "-":
                # then we check around if it's a number
                if index > 0:
                    if not str_indice[index-1].isnumeric():
                        return "- should be surrounding by numbers"
                else:
                    return "- can't be the first character"
                if index < n_char -1:
                    if not str_indice[index + 1].isnumeric():
                        return "- should be surrounding by numbers"
                else:
                    return "- can't be the last character"
            elif not character.isnumeric():
                return f"{character} is not an authorised character"

    return True


def extract_indices_from_string(str_indices):
    """
    Take a string representing a serie of indices (int) and return a list of those integers
    There should be only '-' and numbers
    Ex: if str_indices is "1-3 25 6" it returns [1, 2, 3, 25, 6]
    If the format is incorrect, it returns an empty list
    Args:
        str_indices: (str)

    Returns:

    """
    validation = validate_indices_in_string_format(str_indices)
    if isinstance(validation, str) or not validation:
        return []

    indices_list = []

    n_char = len(str_indices)
    str_indices_split = str_indices.split()
    for str_indice in str_indices_split:
        if str_indice.count("-") > 0:
            interval_split = str_indice.split("-")
            indices_list.extend(range(int(interval_split[0]), int(interval_split[1])+1))
        else:
            indices_list.append(int(str_indice))

    return indices_list


def class_name_to_module_name(class_name):
    """
    Transform a class name string to a module name string by converting uppercase letters to lowercase,
    inserting underscores before them (unless two uppercase letters follow each other), and before numbers.
    Example: ConvertAbfToNWB -> convert_abf_to_nwb
    :param class_name: string
    :return: transformed string
    """
    # Insert underscore before uppercase letters not preceded by another uppercase letter or at the start,
    # and before digits
    s1 = re.sub(r'(?<!^)(?<![A-Z])(?=[A-Z])', '_', class_name)
    s2 = re.sub(r'(?<!^)(?=\d)', '_', s1)

    return s2.lower()


def module_name_to_class_name(module_name):
    """
    Transform a module name string by removing underscores and converting the following letter to uppercase.
    Example: convert_abf_to_nwb -> ConvertAbfToNwb
    :param module_name: string
    :return: transformed string
    """
    return ''.join(word.capitalize() for word in module_name.split('_'))


def flatten(list):
    """
    Flatten a nested list no matter the nesting level


    Args:
        list (list): List to flatten

    Returns:
        List without nest

    Examples:
        >>> flatten([1,2,[[3,4],5],[7]])
        [1,2,3,4,5,7]
    """

    if isinstance(list, collections.abc.Iterable) and not isinstance(list, (str, bytes)):
        return [item for sublist in list for item in flatten(sublist)]
    else:
        return [list]


def print_info_dict(my_dict):
    for key, data in my_dict.items():
        print(f"- {key}: {data}")
