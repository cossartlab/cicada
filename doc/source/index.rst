==================================
Welcome to CICADA's documentation!
==================================

Calcium imaging pipeline
########################

Contents
========

.. toctree::
    :maxdepth: 1
    :caption: Getting started

    install

.. toctree::
    :maxdepth: 1
    :caption: Overview

    overview_intro
    overview_software_architecture
    overview_pre_processing

.. toctree::
    :maxdepth: 1
    :caption: First and further use

    overview_1st_use
    overview_analysis_further_use

.. toctree::
    :maxdepth: 1
    :caption: Modules

    preprocessing
    analysis
    gui

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
