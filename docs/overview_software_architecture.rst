---------------------
Software architecture
---------------------


From NWB files to exploitable data
------------------------

CICADA's supported data format is NWB:N files, which contains a lot of different data.
When using CICADA, the first thing that happens is retrieving the data of interest, and converting it to exploitable python-friendly format, thanks to wrappers.

The list of wrappers that are already implemented in CICADA allows a large variety of analysis, and can be completed.


From exploitable data to analysis
------------------------

Once the wrappers have retrieved an converted the data of interest, the data can be used in analysis.

Again, some analysis are already implemented, but one could create new analysis and add it to CICADA as a plugin.


Each analysis is represented by a class that inherits CicadaAnalysis (in the module `cicada.analysis <https://pycicada.readthedocs.io/en/latest/analysis.html#module-cicada.analysis.cicada_analysis>`_).

Each CicadaAnalysis instance instanciate an ArgumentAnalysisHandler that communicates with the GUI and handle the arguments that will be passed to the analysis.

Each CicadaAnalaysis instance allows to check the compatibility of the data to analyze though the method ``check_data()``.

The method ``run_analysis()`` will be called to start the analysis.

The abstract class CicadaAnalysisFormatWrapper allows,through inheritance, to write a wrapper for a given data format, such as nwb.
It allows to get the content from specific data format, and adding a new format consists in creating a new instance of CicadaAnalysisFormatWrapper, without any change in the CicadaAnalysis instances.

