---------------------
First time using CICADA
---------------------

Graphical User Interface
------------------------

The GUI uses Qt through QtPy which wrapps PyQt5 and PySide.

Cicada uses 3 GUI to interact with the user.

Load wrappers and analysis
------------------------

The first GUI allows the user to load the set of wrappers and list of analysis he wants to use.

In CICADA v2.0.0, two sets of wrappers are already implemented and can be found here:

- 'path_to_where_to_clone/cicada/src/cicada/analysis/cicada_analysis_nwb_wrapper.py'
- 'path_to_where_to_clone/cicada/src/cicada/analysis/cicada_analysis_nwb_lsens_wrapper.py'

Some analysis are also already implemented and grouped into plugins folders.

- 'path_to_where_to_clone/cicada/src/cicada/analysis/ci_analyses'
- 'path_to_where_to_clone/cicada/src/cicada/analysis/Cossart_lab_plugins'
- 'path_to_where_to_clone/cicada/src/cicada/analysis/Petersen_lab_plugins'

Load NWB files and analysis choice
------------------------

The second GUI is composed of 3 main modules.

1. On the left side of the window, a list that displays the loaded recorded sessions to analyse (`SessionsWidget <https://pycicada.readthedocs.io/en/latest/gui.html#module-cicada.gui.session_show_filter_group>`_).

The user can load NWB files with 'Ctrl+O' or 'File' -> 'Open new dataset...'
It is also possible to add data to the current dataset, or to use groups and filters on the current dataset.

2. In the center, a tree that displays the available analyses, updated depending on the loaded NWB files. (`AnalysisTreeApp <https://pycicada.readthedocs.io/en/latest/gui.html#module-cicada.gui.cicada_analysis_tree_guianalysis>`_)

3. On the right side, a summary that shows the analysis chosen, the NWB files chosen, the progression of the analysis, and a link to open the folder where the results are saved.

Analysis parameters
------------------------

The third GUI also shows different modules. (`AnalysisParametersApp <https://pycicada.readthedocs.io/en/latest/gui.html#module-cicada.gui.cicada_analysis_parameters_gui>`_)

1. On the top left of the window, the analysis chosen and the NWB files chosen are displayed. The user can run the analysis thanks to the corresponding button, and manage the parameters of the analysis. One can save the current parameters with a keyword, load a previously saved set of parameters thanks to the keyword that corresponds to it, load a .yaml file containing a set of parameter, or reset the parameters to default value.

2. On the top right of the window, the user can choose the mentioned parameters of the analysis.

3. In the middle of the window, the progression of the analysis is displayed with a progressbar.

4. At the bottom of the window, a console with execution infos and warning/error infos is displayed.