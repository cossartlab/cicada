---------------------
Implement new analysis
---------------------


An analysis template can be found in cicada/src/cicada/analysis/analysis_template.
New analysis can be created and added as plugins to cicada. The plugins are here: cicada/src/cicada/analysis.