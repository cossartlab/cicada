========
PyCICADA
========

CICADA stands for Calcium Imaging Complete Automated Data Analysis.

It's a Python pipeline aimed for analyzing calcium imaging data.

Our goal is to provide an easy-to-use GUI that allows to load calcium imaging data (supporting NWB format only so far)
and to offer a wide range of analyses with some exploratory tools.

The analysis parameters are easy to set through our GUI and can be saved and re-used any time.
Each time, a log file and all parameters are saved to improve reproducibility.

The toolbox was designed to be flexible. Thus adding a new analysis is done easily in a plugin way.

Other formats that NWB could be added easily, as we used a wrapper system.

CICADA is under development. The version published is an alpha version and the documentation is still not complete.
Don't hesitate to contact us if you want more information.

--------
Overview
--------

.. image:: images/cicada_screenshot.png
    :align: center
    :alt: CICADA screenshot


**To see it in action, check this** `youtube video <https://youtu.be/xgf2RmrGVx0>`_

CICADA also provides an exploratory GUI named BADASS (Behavioral Analysis of Data And Some Surprises) that offers tools
to explore behavior videos and annotate them.

.. image:: images/badass_gui_screenshot.png
    :align: center
    :alt: BADASS screenshot

------------
Installation
------------
CICADA installation is done in 2 steps:

- Install an enviroment (depending on which cicada version to install)
- Install cicada either as a module in the environment or by cloning the source code 

Installation of CICADA v1.1.0 module:
-------------------------

1- Download cicada updated requirements from `here <https://gitlab.com/cossartlab/cicada/-/blob/master/cicada_updated_requirements.txt>`_,
open an anaconda prompt / command prompt and change directories to where the cicada_updated_requirements is

2- Create an environment and install cicada dependencies:

.. code::

    conda create -n cicada_env python=3.6.7
    conda activate cicada_env
    conda install shapely
    conda install -c conda-forge fa2
    pip install -r cicada_updated_requirements.txt
    pip install pandas==0.24.2
    pip install seaborn==0.9.0


3- To install the specific CICADA version 1.1.0 from gitlab run:

.. code::

    pip install git+https://gitlab.com/cossartlab/cicada.git@1.1.0


4- To run CICADA (as a module from gitalb installation) execute :

.. code::

    conda activate cicada_env
    python -m cicada

Installation of CICADA v2.1.0 module (updated environment and GUI):
-------------------------

1- Download cicada updated requirements from `here <https://gitlab.com/cossartlab/cicada/-/blob/master/requirements_cicada_2.0.txt>`_,
open an anaconda prompt / command prompt and change directories to where the requirements_cicada_2.0 is

2- Create an environment and install cicada dependencies:

.. code::

    conda create -n cicada_env python=3.11
    conda activate cicada_env
    conda install shapely
    conda install -c conda-forge fa2
    pip install -r requirements_cicada_2.0.txt


3- To install the specific CICADA version 2.1.0 from gitlab run:

.. code::

    pip install git+https://gitlab.com/cossartlab/cicada.git@2.1.0


4- To run CICADA (as a module from gitalb installation in the environment) execute :

.. code::

    conda activate cicada_env
    python -m cicada


Installation of the latest version of the module (above 2.1.0):
----------

CICADA is under active development, if you have already installed CICADA and want to be sure you are using the latest version of the module in your environment (this requires to install cicada2.0 environment), run the following:

.. code::

    conda activate cicada_env
    pip uninstall pycicada
    pip install git+https://gitlab.com/cossartlab/cicada.git


----------
Clone CICADA repository to access the codes and run it in an installed environment
----------

1- Create an environment and install cicada dependencies:

.. code::

    conda create -n cicada_env python=3.11
    conda activate cicada_env
    conda install shapely
    conda install -c conda-forge fa2
    pip install -r requirements_cicada_2.0.txt

2- Clone the CICADA repository from gitlab:

.. code::

    cd 'path_to_where_to_clone'
    git clone https://gitlab.com/cossartlab/cicada.git

3- Install CICADA from the cloned source code in your environment:

.. code::

    cd path_to_where_to_clone/cicada
    pip install -e .

4- Run CICADA from the cloned source code in your environment:

.. code::

    cd src/cicada
    python __main__.py

See `here <https://pycicada.readthedocs.io/en/latest/install.html#download-cicada-codes>`_ for an example of how to run CICADA from the gitlab clone.

-------------
Documentation
-------------

Documentation (in progress) of CICADA can be found `here <https://cicada-20.readthedocs.io>`_.

-------------
Example cases
-------------

To test CICADA you are invited to use the NWB:N files that can be found `here <https://gui.dandiarchive.org/#/dandiset/000219/draft>`_ on DANDI Archive.

--------
Contacts
--------


- Julien Denis (former PhD student and first/main developer): julien.denis3@gmail.com

- Robin Dard (former PhD student and contributor to CICADA project): dardrobin@gmail.com

- Michel Picardo (Permanent Researcher): michel.picardo@inserm.fr

- Rosa Cossart (Research Director / Team leader): rosa.cossart@inserm.fr

-------
LICENSE
-------

Copyright (c) 2019 Cossart lab

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
